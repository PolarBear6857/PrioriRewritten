# Contributing to Priori

Welcome, and thank you for considering contributing to Priori! This document provides guidelines for contributing to the project and aims to make the contribution process as smooth and enjoyable as possible.

## Getting Started

### Setting Up Your Development Environment

1. **Install .NET 9**: Ensure you have the .NET 9 SDK installed on your machine. You can download it from the [official .NET website](https://dotnet.microsoft.com/en-us/download/dotnet/9.0).

2. **AvaloniaUI 11**: This project uses AvaloniaUI 11 for its UI components. Familiarize yourself with AvaloniaUI by visiting their [official documentation](https://docs.avaloniaui.net/).

3. **JetBrains Rider**: We recommend using JetBrains Rider for development, as it's configured with this project's specific settings. You can download it from the [JetBrains website](https://www.jetbrains.com/rider/).

4. **Clone the Repository**: Clone the project repository to your local machine using Git.

    ```bash
    git clone git@gitlab.com:Anthony0001/PrioriRewritten.git
    ```

5. **Build the Project**: Navigate to the cloned directory and build the project using the .NET CLI.

    ```bash
    dotnet build
    ```

## Contributing Guidelines

### Reporting Issues

- **Use the Issue Tracker**: Submit issues via the project's issue tracker. Please check for existing issues to avoid duplicates.
- **Be Detailed**: Provide as much information as possible, including steps to reproduce the issue, expected behavior, and actual behavior.

### Submitting Changes

1. **Fork the Repository**: Create your own fork of the code.
2. **Create a Branch**: Make your changes in a new git branch.

    ```bash
    git checkout -b my-fix-branch master
    ```

3. **Commit Your Changes**: Follow our [commit message guidelines](#commit-message-guidelines).
4. **Submit a Pull Request**: Push your branch to GitHub and open a pull request against the master branch of the [Your Project Name] repository.

### Coding Standards

- **.NET 9 and C# 11 Practices**: Follow best practices for .NET 9 and C# 11, ensuring code is efficient, readable, and uses the latest features appropriately.
- **AvaloniaUI 11 Guidelines**: Adhere to recommended practices for AvaloniaUI 11, focusing on performance and user experience.

## Commit Message Guidelines

- **Short and Descriptive**: Begin with a brief summary of the change in no more than 50 characters.
- **Body**: Provide a detailed description of the change. Explain the problem it solves and why you chose the solution.

## Additional Resources

- [Official .NET Documentation](https://docs.microsoft.com/en-us/dotnet/)
- [AvaloniaUI Documentation](https://docs.avaloniaui.net/)
- [JetBrains Rider Help](https://www.jetbrains.com/help/rider/)

Thank you for contributing to Priori! We look forward to your contributions.
