# Popis Problému
- Krátký a jasný popis problému.

# Reprodukce Problému
- Kroky k reprodukci chování.

# Očekávané Chování
- Jasný a stručný popis toho, co jste očekávali, že se stane.

# Screenshoty
- Pokud je to možné, přidejte snímky obrazovky, aby váš problém lépe pochopen.

# Verze
- OS: [např. Windows 11 22H2]
- Priori Verze: [např. 2024-03-28]

# Dodatečné Poznámky
- Jakékoli další poznámky nebo komentáře k problému.