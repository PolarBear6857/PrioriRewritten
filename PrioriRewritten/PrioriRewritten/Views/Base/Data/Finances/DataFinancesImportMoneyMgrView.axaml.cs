using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Platform.Storage;
using PrioriRewritten.Managers.Base;
using PrioriRewritten.Managers.Plugins.Finances;
using PrioriRewritten.Models.Plugins.Finances;

namespace PrioriRewritten.Views.Base.Data.Finances;

public partial class DataFinancesImportMoneyMgrView : UserControl
{
    public DataFinancesImportMoneyMgrView()
    {
        InitializeComponent();
    }

    private async void SelectFileButton_OnClick(object? sender, RoutedEventArgs e)
    {
        var topLevel = TopLevel.GetTopLevel(this);
        var file = await topLevel.StorageProvider.OpenFilePickerAsync(new FilePickerOpenOptions
        {
            Title = "Pick a file to import.",
            AllowMultiple = false,
            FileTypeFilter = new[]
            {
                new FilePickerFileType("Excel .xlsx")
                {
                    Patterns = new[] { "*.xlsx" }
                }
            }
        });
        if (DataContext is ViewModels.Base.Data.Finances.DataFinancesImportMoneyMgrView viewModel)
        {
            var data = ExcelReader.ReadExcelFile(file[0].Path);
            viewModel.SharedImportViewModel.ImportedAccounts = FinancesDataProcessingManager.GetAccountsFromData(data);
        }
    }

    private void AccountButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is Button { DataContext: FinancesAccountModel account } &&
            DataContext is ViewModels.Base.Data.Finances.DataFinancesImportMoneyMgrView viewModel)
            viewModel.SharedImportViewModel.SelectedAccount = account;
    }
}