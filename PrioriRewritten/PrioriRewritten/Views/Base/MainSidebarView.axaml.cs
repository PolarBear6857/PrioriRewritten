using Avalonia.Controls;
using Avalonia.Interactivity;
using PrioriRewritten.Managers.Base;
using PrioriRewritten.ViewModels.Base;

namespace PrioriRewritten.Views.Base;

public partial class MainSidebarView : UserControl
{
    private readonly SettingsManager _settingsManager = SettingsManager.Instance;

    public MainSidebarView()
    {
        InitializeComponent();
        if (!_settingsManager.SidebarOpenOnStartup)
        {
                ChangeGrid();
        }
    }

    private void MinimiseSidebarButton_OnClick(object? sender, RoutedEventArgs e)
    {
        ChangeGrid();
    }

    private void ChangeGrid()
    {
        if (LogoGrid.ColumnDefinitions[1].Width == new GridLength(0))
            LogoGrid.ColumnDefinitions[1].Width = new GridLength(1.5, GridUnitType.Star);
        else
            LogoGrid.ColumnDefinitions[1].Width = new GridLength(0);
    }
}