using Avalonia.Controls;
using Avalonia.Interactivity;
using PrioriRewritten.Models.Plugins.Projects;
using PrioriRewritten.ViewModels.Plugins.Projects;

namespace PrioriRewritten.Views.Plugins.Projects;

public partial class ProjectsSidebarView : UserControl
{
    public ProjectsSidebarView()
    {
        InitializeComponent();
    }

    private void ProjectButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is Button { DataContext: ProjectsProjectModel data } &&
            DataContext is ProjectsSidebarViewModel model)
        {
            var index = model.DataViewModel.Projects.IndexOf(data);
            model.DataViewModel.SelectedProject = model.DataViewModel.Projects[index];
            model.DataViewModel.CurrentView = new ProjectsKanbanViewModel();
        }
    }
}