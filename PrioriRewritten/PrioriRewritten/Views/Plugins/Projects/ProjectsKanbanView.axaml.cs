using Avalonia.Controls;
using Avalonia.Interactivity;
using PrioriRewritten.Models.Plugins.Projects;
using PrioriRewritten.ViewModels.Plugins.Projects;

namespace PrioriRewritten.Views.Plugins.Projects;

public partial class ProjectsKanbanView : UserControl
{
    public ProjectsKanbanView()
    {
        InitializeComponent();
    }

    private void AddButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is Button { DataContext: ProjectsKanbanBoardModel data } &&
            DataContext is ProjectsKanbanViewModel model)
        {
            model.ProjectsData.SelectedBoard = data;
        }
    }
}