using System;
using Avalonia.Controls;
using Avalonia.Interactivity;

namespace PrioriRewritten.Views.Plugins.Tasks;

public partial class TasksTimePickerView : UserControl
{
    public TasksTimePickerView()
    {
        InitializeComponent();
    }

    private void Button_OnClick(object? sender, RoutedEventArgs e)
    {
        MyTimePicker.SelectedTime = TimeSpan.Zero;
        Calendar.SelectedDate = DateTime.Today.AddDays(1);
    }
}