using Avalonia.Controls;
using Avalonia.Interactivity;
using PrioriRewritten.Models.Plugins.Tasks;
using PrioriRewritten.ViewModels.Plugins.Tasks;
using PrioriRewritten.ViewModels.Plugins.Tasks.Subviews;

namespace PrioriRewritten.Views.Plugins.Tasks;

public partial class TasksSidebarView : UserControl
{
    public TasksSidebarView()
    {
        InitializeComponent();
    }

    private void ListButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is Button { DataContext: TasksListModel data } && DataContext is TasksSidebarViewModel model)
        {
            model.TasksData.SelectedList = data;
            model.TasksData.CurrentView = new TasksHeadViewModel();
            model.TasksData.SelectedView = new TasksListViewModel();
            model.TasksData.SortTasksToGroups();
        }
    }
}