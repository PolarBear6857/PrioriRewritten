using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using PrioriRewritten.ViewModels.Plugins.Tasks.Subviews;

namespace PrioriRewritten.Views.Plugins.Tasks.Subviews;

public partial class TasksInboxView : UserControl
{
    public TasksInboxView()
    {
        InitializeComponent();
    }

    private void AddTaskTextBox_OnFocus(object? sender, PointerPressedEventArgs pointerPressedEventArgs)
    {
        if (DataContext is TasksInboxViewModel model)
        {
            model.AddTaskBorderThickness = new Thickness(1);
            model.DateButtonVisible = true;
            Box.Focus();
        }
    }


    private void AddTaskTextBox_GotFocus(object? sender, GotFocusEventArgs e)
    {
        if (DataContext is TasksInboxViewModel model)
        {
            model.AddTaskBorderThickness = new Thickness(1);
            model.DateButtonVisible = true;
            Box.Focus();
        }
    }


    private void AddTaskButton_OnClick(object? sender, RoutedEventArgs e)
    {
        AddTask();
    }

    private void Box_OnKeyDown(object? sender, KeyEventArgs e)
    {
        if (e.Key == Key.Enter) AddTask();
    }

    private void AddTask()
    {
        if (DataContext is TasksInboxViewModel model)
        {
            model.AddTask();
            Box.Text = string.Empty;
        }
    }

    private void AddTaskTextBox_LostFocus(object? sender, RoutedEventArgs e)
    {
        // TODO
    }
}