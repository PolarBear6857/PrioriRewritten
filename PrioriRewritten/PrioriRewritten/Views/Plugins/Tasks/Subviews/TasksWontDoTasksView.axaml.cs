using Avalonia.Controls;
using Avalonia.Interactivity;
using PrioriRewritten.Models.Plugins.Tasks;
using PrioriRewritten.ViewModels.Plugins.Tasks;
using PrioriRewritten.ViewModels.Plugins.Tasks.Subviews;

namespace PrioriRewritten.Views.Plugins.Tasks.Subviews;

public partial class TasksWontDoTasksView : UserControl
{
    public TasksWontDoTasksView()
    {
        InitializeComponent();
    }

    private void TaskButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is Button { DataContext: TasksTaskModel data } &&
            DataContext is TasksWontDoTasksViewModel model)
        {
            model.TasksData.SelectedTask = data;
            model.TasksData.SelectedTaskView = new TasksTaskSelectedViewModel();
        }
    }


    private void DeleteItem_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is MenuItem { DataContext: TasksTaskModel data } && DataContext is TasksWontDoTasksViewModel model)
        {
            model.TasksData.SelectedTask = data;
            model.TasksData.DeleteTask();
        }
    }
}