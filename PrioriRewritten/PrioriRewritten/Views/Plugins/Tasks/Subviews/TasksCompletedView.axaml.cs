using Avalonia.Controls;
using Avalonia.Interactivity;
using PrioriRewritten.Models.Plugins.Tasks;
using PrioriRewritten.ViewModels.Plugins.Tasks;
using PrioriRewritten.ViewModels.Plugins.Tasks.Subviews;

namespace PrioriRewritten.Views.Plugins.Tasks.Subviews;

public partial class TasksCompletedView : UserControl
{
    public TasksCompletedView()
    {
        InitializeComponent();
    }


    private void TaskButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is Button { DataContext: TasksTaskModel data } &&
            DataContext is TasksCompletedViewModel model)
        {
            model.TasksData.SelectedTask = data;
            model.TasksData.SelectedTaskView = new TasksTaskSelectedViewModel();
        }
    }

    private void WontDoItem_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is MenuItem { DataContext: TasksTaskModel data } && DataContext is TasksCompletedViewModel model)
        {
            model.TasksData.SelectedTask = data;
            model.TasksData.WontDoTask();
        }
    }

    private void DeleteItem_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is MenuItem { DataContext: TasksTaskModel data } && DataContext is TasksCompletedViewModel model)
        {
            model.TasksData.SelectedTask = data;
            model.TasksData.DeleteTask();
        }
    }
}