using System;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Interactivity;
using PrioriRewritten.Models.Plugins.Tasks;
using PrioriRewritten.ViewModels.Plugins.Tasks;
using PrioriRewritten.ViewModels.Plugins.Tasks.Subviews;
using PrioriRewritten.Views.Plugins.Tasks.Popups;

namespace PrioriRewritten.Views.Plugins.Tasks.Subviews;

public partial class TasksTaskGroupsView : UserControl
{
    public TasksTaskGroupsView()
    {
        InitializeComponent();
    }

    private void TaskButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is Button { DataContext: TasksTaskModel data } &&
            DataContext is TasksTaskGroupsViewModel model)
        {
            model.TasksData.SelectedTask = data;
            model.TasksData.SelectedTaskView = new TasksTaskSelectedViewModel();
        }
    }


    private void WontDoItem_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is MenuItem { DataContext: TasksTaskModel data } && DataContext is TasksTaskGroupsViewModel model)
        {
            model.TasksData.SelectedTask = data;
            model.TasksData.WontDoTask();
        }
    }

    private void DeleteItem_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is MenuItem { DataContext: TasksTaskModel data } && DataContext is TasksTaskGroupsViewModel model)
        {
            model.TasksData.SelectedTask = data;
            model.TasksData.DeleteTask();
        }
    }

    private void ReopenItem_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is MenuItem { DataContext: TasksTaskModel data } && DataContext is TasksTaskGroupsViewModel model)
        {
            model.TasksData.SelectedTask = data;
            model.TasksData.ReopenTask();
        }
    }

    private void RestoreItem_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is MenuItem { DataContext: TasksTaskModel data } && DataContext is TasksTaskGroupsViewModel model)
        {
            model.TasksData.SelectedTask = data;
            model.TasksData.RestoreTask();
        }
    }

    private void EraseItem_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is MenuItem { DataContext: TasksTaskModel data } && DataContext is TasksTaskGroupsViewModel model)
        {
            model.TasksData.SelectedTask = data;
            model.TasksData.EraseTask();
        }
    }

    private async void MoveItem_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is MenuItem { DataContext: TasksTaskModel data } && DataContext is TasksTaskGroupsViewModel model)
        {
            model.TasksData.SelectedTask = data;
            await ShowMoveDialogAsync();
        }
    }

    private async Task ShowMoveDialogAsync()
    {
        var moveDialog = new TasksMoveToListPopupView();
        MainPanel.Children.Add(moveDialog);
        await moveDialog.WaitForDeletionConfirmationAsync();
        MainPanel.Children.Remove(moveDialog);
    }
}