using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using PrioriRewritten.ViewModels.Plugins.Tasks.Subviews;
using PrioriRewritten.Views.Plugins.Tasks.Popups;

namespace PrioriRewritten.Views.Plugins.Tasks.Subviews;

public partial class TasksListView : UserControl
{
    public TasksListView()
    {
        InitializeComponent();
    }

    private void AddTaskTextBox_OnFocus(object? sender, PointerPressedEventArgs pressedEventArgs)
    {
        if (DataContext is TasksListViewModel model)
        {
            model.AddTaskBorderThickness = new Thickness(1);
            model.DateButtonVisible = true;
            Box.Focus();
        }
    }


    private void AddTaskTextBox_GotFocus(object? sender, GotFocusEventArgs e)
    {
        if (DataContext is TasksListViewModel model)
        {
            model.AddTaskBorderThickness = new Thickness(1);
            model.DateButtonVisible = true;
            Box.Focus();
        }
    }

    private void AddTaskButton_OnClick(object? sender, RoutedEventArgs e)
    {
        AddTask();
    }

    private void Box_OnKeyDown(object? sender, KeyEventArgs e)
    {
        if (e.Key == Key.Enter) AddTask();
    }

    private void AddTask()
    {
        if (DataContext is TasksListViewModel model)
        {
            model.AddTask();
            Box.Text = string.Empty;
        }
    }

    private void AddTaskTextBox_LostFocus(object? sender, RoutedEventArgs routedEventArgs)
    {
        // TODO
    }


    private void RenameList_OnClick(object? sender, RoutedEventArgs e)
    {
        if (DataContext is TasksListViewModel model)
        {
            model.CanEditListName = true;
            TextBox.Focus();
        }
    }

    private async void DeleteList_OnClick(object? sender, RoutedEventArgs e)
    {
        bool confirmed = await ShowDeleteDialogAsync();
        if (confirmed && DataContext is TasksListViewModel model)
        {
            int index = model.TasksData.TasksLists.IndexOf(model.TasksData.SelectedList);
            model.TasksData.TasksLists.RemoveAt(index);
            model.TasksData.SelectedList = new();
            model.TasksData.SelectedView = new TasksInboxViewModel();
            model.TasksData.SortTasksToGroups();
        }
    }

    private async Task<bool> ShowDeleteDialogAsync()
    {
        var deleteDialog = new TasksDeleteListPopupView();
        MainPanel.Children.Add(deleteDialog);
        bool confirmed = await deleteDialog.WaitForDeletionConfirmationAsync();
        MainPanel.Children.Remove(deleteDialog);
        return confirmed;
    }

    private void ListOptionsButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is Button { ContextMenu: not null } button)
        {
            button.ContextMenu.Open();
        }
    }

    private void InputElement_OnKeyDown(object? sender, KeyEventArgs e)
    {
        if (e.Key == Key.Enter)
        {
            DisableCanEdit();
        }
    }

    private void InputElement_OnLostFocus(object? sender, RoutedEventArgs e)
    {
        DisableCanEdit();
    }

    private void DisableCanEdit()
    {
        if (DataContext is TasksListViewModel model)
        {
            model.CanEditListName = false;
        }
    }
}