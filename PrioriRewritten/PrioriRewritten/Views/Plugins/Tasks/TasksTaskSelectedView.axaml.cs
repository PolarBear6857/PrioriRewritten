using Avalonia.Controls;
using Avalonia.Interactivity;

namespace PrioriRewritten.Views.Plugins.Tasks;

public partial class TasksTaskSelectedView : UserControl
{
    public TasksTaskSelectedView()
    {
        InitializeComponent();
    }

    private void PriorityButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is Button button && button.ContextMenu != null)
        {
            if (button.ContextMenu.IsOpen)
                button.ContextMenu.Close();
            else
                button.ContextMenu.Open();
        }
    }
}