using Avalonia.Controls;
using Avalonia.Interactivity;
using PrioriRewritten.ViewModels.Plugins.Tasks;
using PrioriRewritten.ViewModels.Plugins.Tasks.Subviews;

namespace PrioriRewritten.Views.Plugins.Tasks;

public partial class TasksAddListView : UserControl
{
    public TasksAddListView()
    {
        InitializeComponent();
    }

    private void CreateButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (DataContext is TasksAddListViewModel model)
        {
            model.TasksData.CurrentView = new TasksHeadViewModel();
            model.TasksData.SelectedView = new TasksListViewModel();
            model.TasksData.SortTasksToGroups();
        }
    }
}