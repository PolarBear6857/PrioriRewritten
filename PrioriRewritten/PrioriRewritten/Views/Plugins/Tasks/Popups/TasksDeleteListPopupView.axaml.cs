using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Interactivity;

namespace PrioriRewritten.Views.Plugins.Tasks.Popups;

public partial class TasksDeleteListPopupView : UserControl
{
    private TaskCompletionSource<bool> _tcs = new();

    public TasksDeleteListPopupView()
    {
        InitializeComponent();
    }

    private void ConfirmDeleteButton_OnClick(object sender, RoutedEventArgs e)
    {
        _tcs.TrySetResult(true);
        IsVisible = false;
    }
    

    public Task<bool> WaitForDeletionConfirmationAsync()
    {
        return _tcs.Task;
    }

    private void CancelButton_OnClick(object? sender, RoutedEventArgs e)
    {
        _tcs.TrySetResult(false);
        IsVisible = false;
    }
}