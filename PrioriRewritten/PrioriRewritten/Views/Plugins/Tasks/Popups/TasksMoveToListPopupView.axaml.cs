using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Interactivity;
using PrioriRewritten.ViewModels.Plugins.Tasks.Popups;

namespace PrioriRewritten.Views.Plugins.Tasks.Popups;

public partial class TasksMoveToListPopupView : UserControl
{
    private TaskCompletionSource<bool> _tcs = new();

    public TasksMoveToListPopupView()
    {
        InitializeComponent();
        DataContext = new TasksMoveToListPopupViewModel();
    }

    private void HideButton_OnClick(object? sender, RoutedEventArgs e)
    {
        _tcs.TrySetResult(true);
        IsVisible = false;
    }

    public Task<bool> WaitForDeletionConfirmationAsync()
    {
        return _tcs.Task;
    }
}