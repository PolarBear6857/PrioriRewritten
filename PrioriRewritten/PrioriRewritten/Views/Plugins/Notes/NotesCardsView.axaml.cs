using Avalonia.Controls;
using Avalonia.Interactivity;
using PrioriRewritten.Models.Plugins.Notes;
using PrioriRewritten.ViewModels.Plugins.Notes;

namespace PrioriRewritten.Views.Plugins.Notes;

public partial class NotesCardsView : UserControl
{
    public NotesCardsView()
    {
        InitializeComponent();
    }

    private void EditButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is MenuItem { DataContext: NotesNoteModel data } && DataContext is NotesCardsViewModel model)
        {
            model.NotesData.SelectedNote = data;
            model.NotesData.CurrentView = new NotesEditNoteViewModel();
        }
    }

    private void ContextButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is Control { ContextMenu: not null } control)
        {
            if (control.ContextMenu.IsOpen)
                control.ContextMenu.Close();
            else
                control.ContextMenu.Open();
        }
    }

    private void DeleteButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is MenuItem { DataContext: NotesNoteModel data } && DataContext is NotesCardsViewModel model)
        {
            model.NotesData.SelectedNote = data;
            model.NotesData.AllNotes.Remove(model.NotesData.SelectedNote);
        }
    }
}