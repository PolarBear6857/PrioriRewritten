using Avalonia.Controls;
using Avalonia.Interactivity;
using PrioriRewritten.Models.Plugins.Notes;
using PrioriRewritten.ViewModels.Plugins.Notes;

namespace PrioriRewritten.Views.Plugins.Notes;

public partial class NotesListView : UserControl
{
    public NotesListView()
    {
        InitializeComponent();
    }

    private void EditButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is MenuItem { DataContext: NotesNoteModel data } && DataContext is NotesListViewModel model)
        {
            model.NotesData.SelectedNote = data;
            model.NotesData.CurrentView = new NotesEditNoteViewModel();
        }
    }

    private void DeleteButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is MenuItem { DataContext: NotesNoteModel data } && DataContext is NotesListViewModel model)
        {
            model.NotesData.SelectedNote = data;
            model.NotesData.AllNotes.Remove(model.NotesData.SelectedNote);
        }
    }

    private void ContextButton_OnClick(object? sender, RoutedEventArgs routedEventArgs)
    {
        if (sender is Button { ContextMenu: not null } control)
        {
            if (control.ContextMenu.IsOpen)
                control.ContextMenu.Close();
            else
                control.ContextMenu.Open();
        }
    }
}