using Avalonia.Controls;
using Avalonia.Interactivity;
using PrioriRewritten.Models.Plugins.Notes;
using PrioriRewritten.ViewModels.Plugins.Notes;

namespace PrioriRewritten.Views.Plugins.Notes;

public partial class NotesEditNoteView : UserControl
{
    public NotesEditNoteView()
    {
        InitializeComponent();
    }

    private void TagDeleteButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is Button { DataContext: NotesTagModel data } && DataContext is NotesEditNoteViewModel model)
            model.NotesData.SelectedNote.Tags.Remove(data);
    }

    private void ContextButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is Control { ContextMenu: not null } control)
        {
            if (control.ContextMenu.IsOpen)
                control.ContextMenu.Close();
            else
                control.ContextMenu.Open();
        }
    }

    private void DeleteButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is MenuItem && DataContext is NotesEditNoteViewModel model)
        {
            model.NotesData.AllNotes.Remove(model.NotesData.SelectedNote);
            var tmpModel = new NotesHeadViewModel();
            tmpModel.Initialize();
        }
    }
}