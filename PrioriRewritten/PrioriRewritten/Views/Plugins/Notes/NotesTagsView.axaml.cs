using System.Linq;
using Avalonia.Controls;
using Avalonia.Interactivity;
using PrioriRewritten.Models.Plugins.Notes;
using PrioriRewritten.ViewModels.Plugins.Notes;

namespace PrioriRewritten.Views.Plugins.Notes;

public partial class NotesTagsView : UserControl
{
    public NotesTagsView()
    {
        InitializeComponent();
    }

    private void TagDeleteButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is Button { DataContext: NotesTagModel data } && DataContext is NotesTagsViewModel model)
        {
            foreach (var note in model.NotesData.AllNotes)
            {
                var tagsToRemove = note.Tags.Where(tag => tag.Name == data.Name).ToList();
                foreach (var tagToRemove in tagsToRemove) note.Tags.Remove(tagToRemove);
            }

            model.NotesData.AllTags.Remove(data);
        }
    }


    private void TagAddButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is Button { DataContext: NotesTagModel data } && DataContext is NotesTagsViewModel model)
        {
            foreach (var tag in model.NotesData.SelectedNote.Tags)
                if (data.Name == tag.Name)
                    return;

            model.NotesData.SelectedNote.Tags.Add(data);
        }
    }
}