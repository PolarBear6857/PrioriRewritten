using Avalonia.Controls;
using Avalonia.Interactivity;
using PrioriRewritten.Models.Plugins.Media;
using PrioriRewritten.ViewModels.Plugins.Media;

namespace PrioriRewritten.Views.Plugins.Media;

public partial class MediaSidebarView : UserControl
{
    public MediaSidebarView()
    {
        InitializeComponent();
    }

    private void LibraryButton_OnClick(object? sender, RoutedEventArgs e)
    {
        if (sender is Button { DataContext: MediaLibraryModel library } &&
            DataContext is MediaSidebarViewModel viewModel)
        {
            viewModel.SharedViewModel.SelectedLibrary = library;
            viewModel.SharedViewModel.ChangeViewSelectedLibrary();
        }
    }
}