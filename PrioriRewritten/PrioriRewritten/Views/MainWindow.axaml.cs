using Avalonia.Controls;
using PrioriRewritten.Managers.Base;
using PrioriRewritten.Models.Base;

namespace PrioriRewritten.Views;

public partial class MainWindow : Window
{
    public MainWindow()
    {
        InitializeComponent();
        //ExcelReader.ReadExcelFile();
        FileManager.CreateBaseFolderStructure();
        SettingsManager.Instance.LoadSettingsFromFile();
    }
}