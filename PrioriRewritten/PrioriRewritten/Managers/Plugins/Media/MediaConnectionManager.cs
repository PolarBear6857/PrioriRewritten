using System.Threading.Tasks;
using PrioriRewritten.Models.Plugins.Media;
using PrioriRewritten.Models.Plugins.Media.Connections;

namespace PrioriRewritten.Managers.Plugins.Media;

public static class MediaConnectionManager
{
    public static async Task<bool> TestConnection(MediaAccountModel accountModel)
    {
        var connection = MediaConnectionFactory.CreateConnection(accountModel.Type);
        connection.Name = accountModel.Name;
        connection.Url = accountModel.Url;
        connection.Username = accountModel.Username;
        connection.Password = accountModel.Password;
        connection.ApiKey = accountModel.ApiKey;

        return await connection.TestConnectionAsync();
    }
}