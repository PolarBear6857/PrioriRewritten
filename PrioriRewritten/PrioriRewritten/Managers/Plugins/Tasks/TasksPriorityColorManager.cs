using Avalonia.Media;
using PrioriRewritten.Models.Plugins.Tasks.Enums;

namespace PrioriRewritten.Managers.Plugins.Tasks;

public class TasksPriorityColorManager
{
    public static Color GetPriorityColor(TasksPriority priority)
    {
        return priority switch
        {
            TasksPriority.High => Colors.Red,
            TasksPriority.Medium => Colors.Orange,
            _ => Colors.White
        };
    }
}