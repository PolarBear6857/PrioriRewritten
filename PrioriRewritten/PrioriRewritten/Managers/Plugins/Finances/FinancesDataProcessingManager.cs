using System;
using System.Collections.Generic;
using System.Data;
using PrioriRewritten.Models.Plugins.Finances;
using PrioriRewritten.Models.Plugins.Finances.Enums;

namespace PrioriRewritten.Managers.Plugins.Finances;

public class FinancesDataProcessingManager
{
    public static List<FinancesAccountModel> GetAccountsFromData(DataSet data)
    {
        List<FinancesAccountModel> uniqueAccounts = new();
        for (var i = 1; i < data.Tables[0].Rows.Count; i++)
        {
            var accountName = data.Tables[0].Rows[i][1].ToString();
            var accountNotFound = true;
            foreach (var acc in uniqueAccounts)
                if (accountName == acc.Name)
                {
                    accountNotFound = false;
                    AddTransactionToAccountFromRow(acc, data.Tables[0].Rows[i]);
                }

            if (accountNotFound)
            {
                FinancesAccountModel newAccount = new()
                {
                    Name = accountName
                };
                uniqueAccounts.Add(newAccount);
                AddTransactionToAccountFromRow(newAccount, data.Tables[0].Rows[i]);
            }
        }

        return uniqueAccounts;
    }

    private static void AddTransactionToAccountFromRow(FinancesAccountModel acc, DataRow row)
    {
        FinancesTransactionModel newTransaction = new()
        {
            Date = DateTime.Parse(row[0].ToString()),
            Description = row[4].ToString(),
            Amount = decimal.Parse(row[8].ToString())
        };
        var transactionType = row[6].ToString();
        if (transactionType == "Income")
            newTransaction.Type = FinancesTransactionEnum.Deposit;
        else if (transactionType == "Exp.") newTransaction.Type = FinancesTransactionEnum.Withdraw;

        acc.Transactions.Add(newTransaction);
    }
}