using System;
using System.IO;
using System.Text.Json;
using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.Models.Base;

namespace PrioriRewritten.Managers.Base;

public class SettingsManager : ObservableObject
{
    private bool _canShowFinances = true;
    private bool _canShowMedia = true;
    private bool _canShowNotes = true;
    private bool _canShowProjects = true;
    private bool _canShowRss = true;
    private bool _canShowTasks = true;
    private bool _sidebarOpenOnStartup = true;
    public static SettingsManager Instance { get; } = new();
    private readonly string _settingsPath = $"{FileManager.ExecutableDirectory}/data/base/settings/settings.json";

    public bool CanShowFinances
    {
        get => _canShowFinances;
        set
        {
            SetProperty(ref _canShowFinances, value);
            SaveSettingsToFile();
        }
    }

    public bool CanShowMedia
    {
        get => _canShowMedia;
        set
        {
            SetProperty(ref _canShowMedia, value);
            SaveSettingsToFile();
        }
    }

    public bool CanShowNotes
    {
        get => _canShowNotes;
        set
        {
            SetProperty(ref _canShowNotes, value);
            SaveSettingsToFile();
        }
    }

    public bool CanShowProjects
    {
        get => _canShowProjects;
        set
        {
            SetProperty(ref _canShowProjects, value);
            SaveSettingsToFile();
        }
    }

    public bool CanShowRss
    {
        get => _canShowRss;
        set
        {
            SetProperty(ref _canShowRss, value);
            SaveSettingsToFile();
        }
    }

    public bool CanShowTasks
    {
        get => _canShowTasks;
        set
        {
            SetProperty(ref _canShowTasks, value);
            SaveSettingsToFile();
        }
    }

    public bool SidebarOpenOnStartup
    {
        get => _sidebarOpenOnStartup;
        set
        {
            SetProperty(ref _sidebarOpenOnStartup, value);
            SaveSettingsToFile();
        }
    }

    public void LoadSettingsFromFile()
    {
        try
        {
            if (!File.Exists(_settingsPath))
            {
                SaveSettingsToFile();
            }

            using var stream = File.OpenRead(_settingsPath);
            var settings = JsonSerializer.Deserialize<SettingsManager>(stream);

            if (settings == null) throw new InvalidOperationException("Failed to deserialize settings.");

            _canShowFinances = settings.CanShowFinances;
            _canShowMedia = settings.CanShowMedia;
            _canShowNotes = settings.CanShowNotes;
            _canShowProjects = settings.CanShowProjects;
            _canShowRss = settings.CanShowRss;
            _canShowTasks = settings.CanShowTasks;
            _sidebarOpenOnStartup = settings.SidebarOpenOnStartup;
            OnPropertyChanged();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Failed to load settings: {ex.Message}");
        }
    }


    private void SaveSettingsToFile()
    {
        try
        {
            var directory = Path.GetDirectoryName(_settingsPath);
            if (directory != null && !Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            using var stream = File.Create(_settingsPath);
            JsonSerializer.Serialize(stream, this, new JsonSerializerOptions { WriteIndented = true });
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Failed to save settings: {ex.Message}");
        }
    }
}