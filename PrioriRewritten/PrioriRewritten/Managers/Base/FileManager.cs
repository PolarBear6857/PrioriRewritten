using System;
using System.IO;

namespace PrioriRewritten.Models.Base;

public class FileManager
{
    public static string ExecutableDirectory = AppDomain.CurrentDomain.BaseDirectory;
    public static string ConfigDirectory = AppDomain.CurrentDomain.BaseDirectory;

    public static void CreateBaseFolderStructure()
    {
        var dataDirectory = $"{ExecutableDirectory}/data";
        var baseDataDirectory = $"{dataDirectory}/base";
        var pluginDataDirectory = $"{dataDirectory}/plugins";
        var tasksPluginDataDirectory = $"{pluginDataDirectory}/tasks";
        var projectsPluginDataDirectory = $"{pluginDataDirectory}/projects";
        var notesPluginDataDirectory = $"{pluginDataDirectory}/notes";
        var financesPluginDataDirectory = $"{pluginDataDirectory}/finances";
        var mediaPluginDataDirectory = $"{pluginDataDirectory}/media";
        var rssPluginDataDirectory = $"{pluginDataDirectory}/rss";
        if (!Directory.Exists(dataDirectory)) Directory.CreateDirectory(dataDirectory);

        if (!Directory.Exists(baseDataDirectory)) Directory.CreateDirectory(baseDataDirectory);

        if (!Directory.Exists(pluginDataDirectory)) Directory.CreateDirectory(pluginDataDirectory);

        if (!Directory.Exists(tasksPluginDataDirectory)) Directory.CreateDirectory(tasksPluginDataDirectory);

        if (!Directory.Exists(projectsPluginDataDirectory)) Directory.CreateDirectory(projectsPluginDataDirectory);

        if (!Directory.Exists(notesPluginDataDirectory)) Directory.CreateDirectory(notesPluginDataDirectory);

        if (!Directory.Exists(financesPluginDataDirectory)) Directory.CreateDirectory(financesPluginDataDirectory);

        if (!Directory.Exists(mediaPluginDataDirectory)) Directory.CreateDirectory(mediaPluginDataDirectory);

        if (!Directory.Exists(rssPluginDataDirectory)) Directory.CreateDirectory(rssPluginDataDirectory);
    }
}