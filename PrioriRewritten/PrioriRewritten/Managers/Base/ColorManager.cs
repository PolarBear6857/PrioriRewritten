using System.Collections.ObjectModel;
using Avalonia.Media;
using PrioriRewritten.Models.Base;

namespace PrioriRewritten.Managers.Base;

public class ColorManager
{
    public static ColorManager Instance { get; } = new();

    public ObservableCollection<AccentColor> AccentColors { get; } =
    [
        new AccentColor("Brand", Color.Parse("#EC7D10")),
        new AccentColor("Orange", Colors.Orange),
        new AccentColor("Red", Colors.Red),
        new AccentColor("Green", Colors.Green),
        new AccentColor("Blue", Colors.Blue),
        new AccentColor("Yellow", Colors.Yellow),
        new AccentColor("Purple", Colors.Purple),
        new AccentColor("Pink", Colors.Pink),
        new AccentColor("Brown", Colors.Brown),
        new AccentColor("Cyan", Colors.Cyan),
        new AccentColor("Magenta", Colors.Magenta),
        new AccentColor("Lime", Colors.Lime),
        new AccentColor("Black", Colors.Black),
        new AccentColor("Olive", Colors.Olive),
        new AccentColor("Teal", Colors.Teal),
        new AccentColor("Navy", Colors.Navy),
        new AccentColor("Maroon", Colors.Maroon),
        new AccentColor("Coral", Colors.Coral),
        new AccentColor("Tomato", Colors.Tomato),
        new AccentColor("Salmon", Colors.Salmon),
        new AccentColor("Lavender", Colors.Lavender),
        new AccentColor("Beige", Colors.Beige)
    ];

    public AccentColor GetColorFromName(string name)
    {
        foreach (var color in AccentColors)
            if (color.Name.ToLower() == name.ToLower())
                return color;

        return null;
    }
}