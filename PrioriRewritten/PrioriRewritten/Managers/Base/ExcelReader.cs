using System;
using System.Data;
using System.IO;
using System.Text;
using ExcelDataReader;

namespace PrioriRewritten.Managers.Base;

public static class ExcelReader
{
    public static DataSet ReadExcelFile(Uri path)
    {
        Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        using var stream = File.Open(path.AbsolutePath, FileMode.Open, FileAccess.Read);
        using var reader = ExcelReaderFactory.CreateReader(stream);
        var data = reader.AsDataSet();
        return data;
    }
}