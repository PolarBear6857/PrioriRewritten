using System;
using System.Globalization;
using Avalonia.Data.Converters;

namespace PrioriRewritten.Converters.Plugins.Tasks;

public class TimeSpanToDateTimeConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        TimeSpan? time = value as TimeSpan?;
        return time?.ToString(@"hh\:mm"); // Format TimeSpan to a string representation
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotSupportedException();
    }
}