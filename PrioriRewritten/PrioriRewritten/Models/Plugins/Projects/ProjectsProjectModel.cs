using System.Collections.ObjectModel;
using CommunityToolkit.Mvvm.ComponentModel;

namespace PrioriRewritten.Models.Plugins.Projects;

public class ProjectsProjectModel : ObservableObject
{
    private ObservableCollection<ProjectsKanbanBoardModel> _boards = new();
    private string _name = "New Project";

    public string Name
    {
        get => _name;
        set => SetProperty(ref _name, value);
    }

    public ObservableCollection<ProjectsKanbanBoardModel> Boards
    {
        get => _boards;
        set => SetProperty(ref _boards, value);
    }
}