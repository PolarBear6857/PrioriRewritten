using CommunityToolkit.Mvvm.ComponentModel;

namespace PrioriRewritten.Models.Plugins.Projects;

public class ProjectsKanbanBoardItemModel : ObservableObject
{
    private string _name = "Item";
    private string _description = string.Empty;

    public string Name
    {
        get => _name;
        set => SetProperty(ref _name, value);
    }

    public string Description
    {
        get => _description;
        set => SetProperty(ref _description, value);
    }
}