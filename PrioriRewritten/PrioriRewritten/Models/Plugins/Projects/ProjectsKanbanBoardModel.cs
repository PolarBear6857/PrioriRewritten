using System.Collections.ObjectModel;
using CommunityToolkit.Mvvm.ComponentModel;

namespace PrioriRewritten.Models.Plugins.Projects;

public class ProjectsKanbanBoardModel : ObservableObject
{
    private ObservableCollection<ProjectsKanbanBoardItemModel> _items = new();
    private string _name = "New Board";
    private ProjectsKanbanBoardItemModel _newItem = new();

    public string Name
    {
        get => _name;
        set => SetProperty(ref _name, value);
    }

    public ObservableCollection<ProjectsKanbanBoardItemModel> Items
    {
        get => _items;
        set => SetProperty(ref _items, value);
    }

    public ProjectsKanbanBoardItemModel NewItem
    {
        get => _newItem;
        set => SetProperty(ref _newItem, value);
    }

    public void AddItem()
    {
        Items.Add(NewItem);
        NewItem = new();
    }
}