using System;
using System.Collections.ObjectModel;
using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.Managers.Base;
using PrioriRewritten.Models.Base;

namespace PrioriRewritten.Models.Plugins.Notes;

public class NotesNoteModel : ObservableObject
{
    private AccentColor _color = ColorManager.Instance.GetColorFromName("Brand");
    private DateTime _dateTimeCreated = DateTime.Now;
    private DateTime _dateTimeModified = DateTime.Now;
    private string _description = string.Empty;
    private string _name = "New Note";
    private bool _readOnly;
    private ObservableCollection<NotesTagModel> _tags = new();

    public string Name
    {
        get => _name;
        set
        {
            SetProperty(ref _name, value);
            DateTimeModified = DateTime.Now;
        }
    }

    public string Description
    {
        get => _description;
        set
        {
            SetProperty(ref _description, value);
            DateTimeModified = DateTime.Now;
        }
    }

    public DateTime DateTimeCreated
    {
        get => _dateTimeCreated;
        set => SetProperty(ref _dateTimeCreated, value);
    }

    public DateTime DateTimeModified
    {
        get => _dateTimeModified;
        set => SetProperty(ref _dateTimeModified, value);
    }

    public AccentColor Color
    {
        get => _color;
        set
        {
            SetProperty(ref _color, value);
            DateTimeModified = DateTime.Now;
        }
    }

    public ObservableCollection<NotesTagModel> Tags
    {
        get => _tags;
        set
        {
            SetProperty(ref _tags, value);
            DateTimeModified = DateTime.Now;
        }
    }

    public bool ReadOnly
    {
        get => _readOnly;
        set
        {
            SetProperty(ref _readOnly, value);
            DateTimeModified = DateTime.Now;
        }
    }
}