using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.Managers.Base;
using PrioriRewritten.Models.Base;

namespace PrioriRewritten.Models.Plugins.Notes;

public class NotesTagModel : ObservableObject
{
    private AccentColor _color = ColorManager.Instance.GetColorFromName("Brand");
    private string _name = string.Empty;

    public string Name
    {
        get => _name;
        set => SetProperty(ref _name, value);
    }

    public AccentColor Color
    {
        get => _color;
        set => SetProperty(ref _color, value);
    }
}