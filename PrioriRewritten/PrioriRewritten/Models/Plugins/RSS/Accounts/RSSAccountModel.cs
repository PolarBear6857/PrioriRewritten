using System;
using System.Threading.Tasks;
using PrioriRewritten.Models.Plugins.RSS.Enums;

namespace PrioriRewritten.Models.Plugins.RSS;

public class RSSAccountModel
{
    public string Name { get; set; } = "New RSS Account";
    public RSSAccountEnums Type { get; set; } = RSSAccountEnums.FreshRSS;
    public string Url { get; set; } = string.Empty;
    public string Username { get; set; } = string.Empty;
    public string Password { get; set; } = string.Empty;
    public string ApiKey { get; set; } = string.Empty;


    public virtual Task<bool> TestConnectionAsync()
    {
        throw new NotImplementedException();
    }
}