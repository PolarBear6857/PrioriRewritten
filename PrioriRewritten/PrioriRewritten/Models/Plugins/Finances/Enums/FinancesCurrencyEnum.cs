namespace PrioriRewritten.Models.Plugins.Finances.Enums;

public enum FinancesCurrencyEnum
{
    CZK,
    EUR,
    USD
}