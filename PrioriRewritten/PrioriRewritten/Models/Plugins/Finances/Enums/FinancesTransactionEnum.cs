namespace PrioriRewritten.Models.Plugins.Finances.Enums;

public enum FinancesTransactionEnum
{
    Deposit,
    Withdraw
}