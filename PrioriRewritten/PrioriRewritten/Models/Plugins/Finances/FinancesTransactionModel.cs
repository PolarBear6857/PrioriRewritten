using System;
using PrioriRewritten.Models.Plugins.Finances.Enums;

namespace PrioriRewritten.Models.Plugins.Finances;

public class FinancesTransactionModel
{
    public FinancesTransactionEnum Type { get; set; }
    public decimal Amount { get; set; }
    public DateTime Date { get; set; }
    public string? Description { get; set; }
}