using System.Collections.Generic;
using PrioriRewritten.Models.Plugins.Finances.Enums;

namespace PrioriRewritten.Models.Plugins.Finances;

public class FinancesAccountModel
{
    public string Name { get; set; } = "New account";
    public FinancesCurrencyEnum? Currency { get; set; } = FinancesCurrencyEnum.CZK;
    public decimal Balance { get; set; }
    public List<FinancesTransactionModel> Transactions { get; set; } = new();
}