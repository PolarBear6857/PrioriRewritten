using System;
using Avalonia.Media;
using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.Models.Plugins.Tasks.Enums;

namespace PrioriRewritten.Models.Plugins.Tasks;

public class TasksTaskModel : ObservableObject
{
    private SolidColorBrush _color = new(Colors.White);
    private SolidColorBrush _priorityColor;
    private DateTime? _completedDateTime;
    private string _description = string.Empty;
    private DateTime? _dueDateTime = DateTime.Today.AddDays(1);
    private TasksPriority _priority = TasksPriority.None;
    private TasksStatus _status = TasksStatus.Doing;
    private string _title = string.Empty;


    public string Title
    {
        get => _title;
        set => SetProperty(ref _title, value);
    }

    public string Description
    {
        get => _description;
        set => SetProperty(ref _description, value);
    }

    private TasksStatus Status
    {
        get => _status;
        set => SetProperty(ref _status, value);
    }

    public DateTime? DueDateTime
    {
        get => _dueDateTime;
        set => SetProperty(ref _dueDateTime, value);
    }

    public DateTime CreatedDateTime { get; } = DateTime.Now;

    public TimeZoneInfo TimeZone { get; } = TimeZoneInfo.Local;

    public bool IsCompleted
    {
        get => Status == TasksStatus.Done;
        set
        {
            if (value)
            {
                CompletedDateTime = DateTime.Now;
                Status = TasksStatus.Done;
                Color = new SolidColorBrush(Colors.ForestGreen);
            }
            else
            {
                CompletedDateTime = null;
                Status = TasksStatus.Doing;
                Color = new SolidColorBrush(Colors.White);
            }

            OnPropertyChanged();
        }
    }

    public bool IsWontDo
    {
        get => Status == TasksStatus.WontDo;
        set
        {
            if (value)
            {
                CompletedDateTime = DateTime.Now;
                Status = TasksStatus.WontDo;
                Color = new SolidColorBrush(Colors.Gray);
            }
            else
            {
                CompletedDateTime = null;
                Status = TasksStatus.Doing;
                Color = new SolidColorBrush(Colors.White);
            }
        }
    }

    public bool IsWontDoorIsDeleted => IsWontDo || IsDeleted;

    public bool IsCompletedOrIsWontDoorIsDeleted => IsCompleted || IsWontDo || IsDeleted;

    public DateTime? CompletedDateTime
    {
        get => _completedDateTime;
        private set => SetProperty(ref _completedDateTime, value);
    }

    public TasksPriority Priority
    {
        get => _priority;
        set
        {
            SetProperty(ref _priority, value);
            switch (Priority)
            {
                case TasksPriority.None:
                {
                    PriorityColor = new(Colors.Transparent);
                    break;
                }
                case TasksPriority.Low:
                {
                    PriorityColor = new(Colors.Blue);
                    break;
                }
                case TasksPriority.Medium:
                {
                    PriorityColor = new(Colors.Orange);
                    break;
                }
                case TasksPriority.High:
                {
                    PriorityColor = new(Colors.Red);
                    break;
                }
            }
        }
    }

    public bool IsDeleted
    {
        get => Status == TasksStatus.Deleted;
        set
        {
            if (value)
            {
                CompletedDateTime = DateTime.Now;
                Status = TasksStatus.Deleted;
                Color = new SolidColorBrush(Colors.IndianRed);
            }
            else
            {
                CompletedDateTime = null;
                Status = TasksStatus.Doing;
                Color = new SolidColorBrush(Colors.White);
            }
        }
    }

    public SolidColorBrush Color
    {
        get => _color;
        set => SetProperty(ref _color, value);
    }

    public SolidColorBrush PriorityColor
    {
        get => _priorityColor;
        set => SetProperty(ref _priorityColor, value);
    }

    public bool IsToday => DueDateTime != null && DueDateTime.Value.Date == DateTime.Today;

    public bool IsTomorrow => DueDateTime != null && DueDateTime.Value.Date == DateTime.Today.AddDays(1);

    public override string ToString()
    {
        return $"Completed {IsCompleted}, WontDo {IsWontDo}, Deleted {IsDeleted}";
    }
}