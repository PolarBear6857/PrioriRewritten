namespace PrioriRewritten.Models.Plugins.Tasks.Enums;

public enum TasksStatus : sbyte
{
    Doing = 0,
    Done = 1,
    WontDo = -1,
    Deleted = -2
}