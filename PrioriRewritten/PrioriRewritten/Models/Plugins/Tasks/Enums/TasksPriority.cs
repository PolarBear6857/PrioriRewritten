namespace PrioriRewritten.Models.Plugins.Tasks.Enums;

public enum TasksPriority
{
    None,
    Low,
    Medium,
    High
}