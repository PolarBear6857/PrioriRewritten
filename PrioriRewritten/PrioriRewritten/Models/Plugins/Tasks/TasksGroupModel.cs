using System.Collections.ObjectModel;
using CommunityToolkit.Mvvm.ComponentModel;

namespace PrioriRewritten.Models.Plugins.Tasks;

public class TasksGroupModel : ObservableObject
{
    private string _name = string.Empty;
    private ObservableCollection<TasksTaskModel> _tasks = new();

    public string Name
    {
        get => _name;
        set => SetProperty(ref _name, value);
    }

    public ObservableCollection<TasksTaskModel> Tasks
    {
        get => _tasks;
        set => SetProperty(ref _tasks, value);
    }
}