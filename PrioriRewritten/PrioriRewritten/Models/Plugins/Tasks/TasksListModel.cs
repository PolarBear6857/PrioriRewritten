using System.Collections.ObjectModel;
using CommunityToolkit.Mvvm.ComponentModel;

namespace PrioriRewritten.Models.Plugins.Tasks;

public class TasksListModel : ObservableObject
{
    private string _name = "New List";
    private ObservableCollection<TasksGroupModel> _taskGroups = new();
    private ObservableCollection<TasksTaskModel> _tasks = new();
    private int _taskCount;

    public string Name
    {
        get => _name;
        set => SetProperty(ref _name, value);
    }

    public ObservableCollection<TasksTaskModel> Tasks
    {
        get => _tasks;
        set => SetProperty(ref _tasks, value);
    }

    public ObservableCollection<TasksGroupModel> TasksGroups
    {
        get => _taskGroups;
        set => SetProperty(ref _taskGroups, value);
    }

    public int TaskCount
    {
        get => _taskCount;
        set => SetProperty(ref _taskCount, value);
    }
}