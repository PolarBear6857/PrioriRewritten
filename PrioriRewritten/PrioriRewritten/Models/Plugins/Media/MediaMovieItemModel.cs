using System;
using System.Collections.Generic;

namespace PrioriRewritten.Models.Plugins.Media;

public class MediaMovieItemModel : MediaItemBaseModel
{
    private List<MediaAudioLanguageModel> _audioTracks;
    private DateTime _dateAdded;
    private string _name;
    private DateTime _releaseDate;
    private string _resolution;
    private List<MediaTagModel> _tags;
}