namespace PrioriRewritten.Models.Plugins.Media.Enums;

public enum MediaConnectionStatusEnums
{
    Unknown,
    Error,
    Ok
}