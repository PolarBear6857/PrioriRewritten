using System;
using System.Collections.Generic;

namespace PrioriRewritten.Models.Plugins.Media;

public class MediaLibraryModel(string name, MediaTypeEnums type)
{
    private List<MediaItemBaseModel> _items = new();

    public string Name => name;

    public List<MediaItemBaseModel> Items
    {
        get => _items;
        set => _items = value ?? throw new ArgumentNullException(nameof(value));
    }

    public MediaTypeEnums Type => type;
}