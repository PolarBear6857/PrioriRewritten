using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace PrioriRewritten.Models.Plugins.Media.Connections;

public class MediaAccountJellyfin : MediaAccountModel
{
    public override async Task<bool> TestConnectionAsync()
    {
        using var httpClient = new HttpClient();
        try
        {
            httpClient.BaseAddress = new Uri(Url);
            var authData = new { Username, Password };
            var authContent = new StringContent(JsonSerializer.Serialize(authData), Encoding.UTF8, "application/json");
            var response = await httpClient.PostAsync("/Users/AuthenticateByName", authContent);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Authentication Successful");
                return true;
            }

            var responseContent = await response.Content.ReadAsStringAsync();
            Console.WriteLine($"Authentication Failed: {response.StatusCode}, Content: {responseContent}");
            return false;
        }
        catch (Exception e)
        {
            Console.WriteLine($"Exception caught: {e.Message}");
            return false;
        }
    }
}