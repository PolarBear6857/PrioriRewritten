using System;
using System.Threading.Tasks;
using PrioriRewritten.Models.Plugins.Media.Enums;

namespace PrioriRewritten.Models.Plugins.Media.Connections;

public class MediaAccountModel
{
    public string Name { get; set; } = "New Connection";
    public MediaConnectionEnums Type { get; set; } = MediaConnectionEnums.Jellyfin;
    public string Url { get; set; } = string.Empty;
    public string Username { get; set; } = string.Empty;
    public string Password { get; set; } = string.Empty;
    public string ApiKey { get; set; } = string.Empty;


    public virtual Task<bool> TestConnectionAsync()
    {
        throw new NotImplementedException();
    }
}