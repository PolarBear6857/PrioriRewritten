using System;
using System.Drawing;

namespace PrioriRewritten.Models.Plugins.Media;

public class MediaTagModel(string name, Color color)
{
    private string _name = name;
    public Color Color { get; set; } = color;

    public string Name
    {
        get => _name;
        set => _name = value ?? throw new ArgumentNullException(nameof(value));
    }
}