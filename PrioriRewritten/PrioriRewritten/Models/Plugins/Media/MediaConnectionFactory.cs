using System;
using PrioriRewritten.Models.Plugins.Media.Connections;
using PrioriRewritten.Models.Plugins.Media.Enums;

namespace PrioriRewritten.Models.Plugins.Media;

public static class MediaConnectionFactory
{
    public static MediaAccountModel CreateConnection(MediaConnectionEnums type)
    {
        return type switch
        {
            MediaConnectionEnums.Jellyfin => new MediaAccountJellyfin(),
            _ => throw new NotImplementedException($"Unsupported type: {type}")
        };
    }
}