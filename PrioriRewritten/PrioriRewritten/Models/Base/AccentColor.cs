using System;
using Avalonia.Media;
using CommunityToolkit.Mvvm.ComponentModel;

namespace PrioriRewritten.Models.Base;

public class AccentColor : ObservableObject
{
    private SolidColorBrush _dark;
    private SolidColorBrush _light;
    private string _name;

    public AccentColor(string name, Color color)
    {
        Name = name;
        Dark = new SolidColorBrush(color);
        Light = LightenColor(color);
    }

    public AccentColor(string name, Color dark, Color light)
    {
        Name = name;
        Dark = new SolidColorBrush(dark);
        Light = new SolidColorBrush(light);
    }

    public string Name
    {
        get => _name;
        set => SetProperty(ref _name, value);
    }

    public SolidColorBrush Dark
    {
        get => _dark;
        set => SetProperty(ref _dark, value);
    }

    public SolidColorBrush Light
    {
        get => _light;
        set => SetProperty(ref _light, value);
    }

    private SolidColorBrush LightenColor(Color color)
    {
        var factor = 0.5;

        var r = (byte)Math.Clamp(color.R + (255 - color.R) * factor, 0, 255);
        var g = (byte)Math.Clamp(color.G + (255 - color.G) * factor, 0, 255);
        var b = (byte)Math.Clamp(color.B + (255 - color.B) * factor, 0, 255);

        return new SolidColorBrush(Color.FromArgb(color.A, r, g, b));
    }
}