using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.Managers.Base;

namespace PrioriRewritten.ViewModels.Base;

public partial class HelpViewModel : ViewModelBase
{
    private readonly MainSharedViewModel _sharedViewModel = MainSharedViewModel.Instance;

    [RelayCommand]
    private void Back()
    {
        _sharedViewModel.CurrentView = new SettingsViewModel();
    }

    [RelayCommand]
    private void Readme()
    {
        ApplicationManager.OpenWebsite("https://gitlab.com/Anthony0001/PrioriRewritten/-/blob/master/README.md");
    }

    [RelayCommand]
    private void Licence()
    {
        ApplicationManager.OpenWebsite("https://gitlab.com/Anthony0001/PrioriRewritten/-/blob/master/LICENCE.md");
    }

    [RelayCommand]
    private void Changelog()
    {
        ApplicationManager.OpenWebsite("https://gitlab.com/Anthony0001/PrioriRewritten/-/blob/master/CHANGELOG.md");
    }

    [RelayCommand]
    private void Contributing()
    {
        ApplicationManager.OpenWebsite("https://gitlab.com/Anthony0001/PrioriRewritten/-/blob/master/CONTRIBUTING.md");
    }

    [RelayCommand]
    private void Faq()
    {
        ApplicationManager.OpenWebsite(
            "https://gitlab.com/Anthony0001/PrioriRewritten/-/blob/master/Documentation/FAQ.md?ref_type=heads");
    }

    [RelayCommand]
    private void DevGuide()
    {
        ApplicationManager.OpenWebsite(
            "https://gitlab.com/Anthony0001/PrioriRewritten/-/blob/master/Documentation/DEV_GUIDE.md?ref_type=heads");
    }

    [RelayCommand]
    private void UserGuide()
    {
        ApplicationManager.OpenWebsite(
            "https://gitlab.com/Anthony0001/PrioriRewritten/-/blob/master/Documentation/USER_GUIDE.md?ref_type=heads");
    }
}