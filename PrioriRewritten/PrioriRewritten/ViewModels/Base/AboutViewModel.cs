using System;
using System.Diagnostics;
using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.Managers.Base;

namespace PrioriRewritten.ViewModels.Base;

public partial class AboutViewModel : ViewModelBase
{
    private readonly MainSharedViewModel _sharedViewModel = MainSharedViewModel.Instance;

    [RelayCommand]
    private void Back()
    {
        _sharedViewModel.CurrentView = new SettingsViewModel();
    }

    [RelayCommand]
    private void OpenGithub()
    {
        ApplicationManager.OpenWebsite("https://github.com/RealAnthony0001");
    }

    [RelayCommand]
    private void OpenGitLab()
    {
        ApplicationManager.OpenWebsite("https://gitlab.com/Anthony0001");
    }

    [RelayCommand]
    private void OpenWakaTime()
    {
        ApplicationManager.OpenWebsite("https://wakatime.com/@Anthony0001");
    }

    [RelayCommand]
    private void OpenRepository()
    {
        ApplicationManager.OpenWebsite("https://gitlab.com/Anthony0001/PrioriRewritten");
    }
    
}