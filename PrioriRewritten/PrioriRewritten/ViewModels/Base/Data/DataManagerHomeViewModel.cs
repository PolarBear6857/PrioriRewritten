using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.ViewModels.Base.Data.Finances;

namespace PrioriRewritten.ViewModels.Base.Data;

public partial class DataManagerHomeViewModel : ViewModelBase
{
    private static readonly MainSharedViewModel SharedViewModel = MainSharedViewModel.Instance;

    [RelayCommand]
    private void Back()
    {
        SharedViewModel.CurrentView = new SettingsViewModel();
    }

    [RelayCommand]
    private void ChangeViewImportFinances()
    {
        SharedViewModel.CurrentView = new DataFinancesImportViewModel();
    }

    [RelayCommand]
    private void ChangeViewExportFinances()
    {
        SharedViewModel.CurrentView = new DataFinancesExportViewModel();
    }
}