using CommunityToolkit.Mvvm.Input;

namespace PrioriRewritten.ViewModels.Base.Data.Finances;

public partial class DataFinancesImportViewModel : ViewModelBase
{
    private static readonly MainSharedViewModel SharedViewModel = MainSharedViewModel.Instance;

    [RelayCommand]
    private void Back()
    {
        SharedViewModel.CurrentView = new DataManagerHomeViewModel();
    }

    [RelayCommand]
    private void ChangeViewMoneyMgr()
    {
        if (SharedViewModel.CurrentView is not DataFinancesImportMoneyMgrView)
            SharedViewModel.CurrentView = new DataFinancesImportMoneyMgrView();
    }
}