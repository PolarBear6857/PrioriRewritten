using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.ViewModels.Plugins.Finances;

namespace PrioriRewritten.ViewModels.Base.Data.Finances;

public partial class DataFinancesImportMoneyMgrView : ViewModelBase
{
    private static readonly MainSharedViewModel MainSharedViewModel = MainSharedViewModel.Instance;
    private readonly FinancesSharedViewModel _sharedViewModel = FinancesSharedViewModel.Instance;

    [ObservableProperty]
    private FinancesSharedImportViewModel _sharedImportViewModel = FinancesSharedImportViewModel.Instance;

    [ObservableProperty] private string _status = string.Empty;

    [ObservableProperty] private bool _validFileSelected = true;

    [RelayCommand]
    private void Back()
    {
        MainSharedViewModel.CurrentView = new DataFinancesImportViewModel();
    }

    [RelayCommand]
    private void ImportData()
    {
        SharedImportViewModel.ImportAccount();
        MainSharedViewModel.CurrentView = new FinancesSidebarViewModel();
        _sharedViewModel.CurrentView = new FinancesDashboardViewModel();
    }
}