using CommunityToolkit.Mvvm.Input;

namespace PrioriRewritten.ViewModels.Base.Data.Finances;

public partial class DataFinancesExportViewModel : ViewModelBase
{
    private static readonly MainSharedViewModel SharedViewModel = MainSharedViewModel.Instance;

    [RelayCommand]
    private void Back()
    {
        SharedViewModel.CurrentView = new DataManagerHomeViewModel();
    }
}