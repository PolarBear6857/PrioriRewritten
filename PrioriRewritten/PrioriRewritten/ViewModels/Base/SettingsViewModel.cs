using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.Managers.Base;
using PrioriRewritten.ViewModels.Base.Data;

namespace PrioriRewritten.ViewModels.Base;

public partial class SettingsViewModel : ViewModelBase
{
    [ObservableProperty] private static MainSharedViewModel _sharedViewModel = MainSharedViewModel.Instance;
    [ObservableProperty] private static SettingsManager _settingsManager = SettingsManager.Instance;

    [RelayCommand]
    private void ChangeViewCredits()
    {
        SharedViewModel.CurrentView = new CreditsViewModel();
    }

    [RelayCommand]
    private void ChangeViewHelp()
    {
        SharedViewModel.CurrentView = new HelpViewModel();
    }

    [RelayCommand]
    private void ChangeViewDataManager()
    {
        SharedViewModel.CurrentView = new DataManagerHomeViewModel();
    }

    [RelayCommand]
    private void ChangeViewAbout()
    {
        SharedViewModel.CurrentView = new AboutViewModel();
    }
}