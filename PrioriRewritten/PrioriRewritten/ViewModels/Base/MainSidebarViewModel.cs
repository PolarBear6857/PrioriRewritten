using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.Managers.Base;
using PrioriRewritten.ViewModels.Plugins.Finances;
using PrioriRewritten.ViewModels.Plugins.Media;
using PrioriRewritten.ViewModels.Plugins.Notes;
using PrioriRewritten.ViewModels.Plugins.Projects;
using PrioriRewritten.ViewModels.Plugins.RSS;
using PrioriRewritten.ViewModels.Plugins.Tasks;

namespace PrioriRewritten.ViewModels.Base;

public partial class MainSidebarViewModel : ViewModelBase
{
    [ObservableProperty] private static MainSharedViewModel _sharedViewModel = MainSharedViewModel.Instance;
    [ObservableProperty] private static SettingsManager _settingsManager = SettingsManager.Instance;
    [ObservableProperty] private bool _isPaneOpen = true;
    [ObservableProperty] private int _logoWidth = 40;
    [ObservableProperty] private char _paneOpenArrow = '<';
    [ObservableProperty] private int _sidebarButtonWidth = 120;
    [ObservableProperty] private string _version = "version: 2024-04-11a";

    public MainSidebarViewModel()
    {
        if (!_settingsManager.SidebarOpenOnStartup)
        {
            TrigerPanel();
        }
    }

    [RelayCommand]
    private void TriggerPane()
    {
        TrigerPanel();
    }

    private void TrigerPanel()
    {
        IsPaneOpen = !IsPaneOpen;
        PaneOpenArrow = IsPaneOpen ? '<' : '>';
        SidebarButtonWidth = IsPaneOpen ? 120 : 37;
        LogoWidth = IsPaneOpen ? 40 : 30;
    }

    [RelayCommand]
    private void ChangeViewDashboard()
    {
        if (_sharedViewModel.CurrentView is not DashboardViewModel)
            _sharedViewModel.CurrentView = new DashboardViewModel();
    }

    [RelayCommand]
    private void ChangeViewTasks()
    {
        if (_sharedViewModel.CurrentView is not TasksSidebarViewModel)
            _sharedViewModel.CurrentView = new TasksSidebarViewModel();
    }

    [RelayCommand]
    private void ChangeViewProjects()
    {
        if (_sharedViewModel.CurrentView is not ProjectsSidebarViewModel)
            _sharedViewModel.CurrentView = new ProjectsSidebarViewModel();
    }

    [RelayCommand]
    private void ChangeViewNotes()
    {
        if (_sharedViewModel.CurrentView is not NotesHeadViewModel)
            _sharedViewModel.CurrentView = new NotesHeadViewModel();
    }

    [RelayCommand]
    private void ChangeViewFinances()
    {
        if (_sharedViewModel.CurrentView is not FinancesSidebarViewModel)
            _sharedViewModel.CurrentView = new FinancesSidebarViewModel();
    }


    [RelayCommand]
    private void ChangeViewMedia()
    {
        if (_sharedViewModel.CurrentView is not MediaSidebarViewModel)
            _sharedViewModel.CurrentView = new MediaSidebarViewModel();
    }

    [RelayCommand]
    private void ChangeViewRss()
    {
        if (_sharedViewModel.CurrentView is not RSSSidebarViewModel)
            _sharedViewModel.CurrentView = new RSSSidebarViewModel();
    }

    [RelayCommand]
    private void ChangeViewSettings()
    {
        if (_sharedViewModel.CurrentView is not SettingsViewModel)
            _sharedViewModel.CurrentView = new SettingsViewModel();
    }
}