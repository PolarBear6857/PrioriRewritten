using CommunityToolkit.Mvvm.ComponentModel;

namespace PrioriRewritten.ViewModels.Base;

public partial class MainSharedViewModel : ViewModelBase
{
    [ObservableProperty] private ViewModelBase _currentView = new DashboardViewModel();

    public static MainSharedViewModel Instance { get; } = new();
}