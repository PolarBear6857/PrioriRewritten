using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.ViewModels.Plugins.Notes;
using PrioriRewritten.ViewModels.Plugins.Projects;
using PrioriRewritten.ViewModels.Plugins.Tasks;

namespace PrioriRewritten.ViewModels.Base;

public partial class DashboardViewModel : ViewModelBase
{
    [ObservableProperty] private static TasksDataViewModel _tasksData = TasksDataViewModel.Instance;
    [ObservableProperty] private static NotesDataViewModel _notesData = NotesDataViewModel.Instance;
    [ObservableProperty] private static ProjectsDataViewModel _projectsData = ProjectsDataViewModel.Instance;

    public DashboardViewModel()
    {
        TasksData.CalculateTotalTaskCount();
    }
}