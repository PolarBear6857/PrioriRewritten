using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace PrioriRewritten.ViewModels.Base;

public partial class CreditsViewModel : ViewModelBase
{
    [ObservableProperty] private MainSharedViewModel _sharedViewModel = MainSharedViewModel.Instance;

    [RelayCommand]
    private void Back()
    {
        SharedViewModel.CurrentView = new SettingsViewModel();
    }
}