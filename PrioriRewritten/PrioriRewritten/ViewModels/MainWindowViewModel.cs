﻿using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.ViewModels.Base;

namespace PrioriRewritten.ViewModels;

public partial class MainWindowViewModel : ViewModelBase
{
    [ObservableProperty] private ViewModelBase _currentView = new MainSidebarViewModel();
}