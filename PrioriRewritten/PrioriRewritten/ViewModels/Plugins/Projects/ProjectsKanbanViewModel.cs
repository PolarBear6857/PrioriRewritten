using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.Models.Plugins.Projects;

namespace PrioriRewritten.ViewModels.Plugins.Projects;

public partial class ProjectsKanbanViewModel : ViewModelBase
{
    [ObservableProperty] private ProjectsDataViewModel _projectsData = ProjectsDataViewModel.Instance;

    public ProjectsKanbanViewModel()
    {
        if (ProjectsData.SelectedProject.Boards.Count == 0)
            ProjectsData.CurrentSubView = new ProjectsNoListViewModel();
        else
            ProjectsData.CurrentSubView = new ProjectsListsViewModel();
    }

    [RelayCommand]
    private void AddBoard()
    {
        ProjectsData.SelectedProject.Boards.Add(new ProjectsKanbanBoardModel());
        if (ProjectsData.SelectedProject.Boards.Count > 0) ProjectsData.CurrentSubView = new ProjectsListsViewModel();
    }

    [RelayCommand]
    private void AddItem()
    {
        ProjectsData.SelectedBoard.AddItem();
    }
}