using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace PrioriRewritten.ViewModels.Plugins.Projects;

public partial class ProjectsAddProjectViewModel : ViewModelBase
{
    [ObservableProperty] private bool _canAddProject = true;
    [ObservableProperty] private ProjectsDataViewModel _dataViewModel = ProjectsDataViewModel.Instance;

    public string Name
    {
        get => DataViewModel.NewProject.Name;
        set
        {
            CanAddProject = value.Trim() != string.Empty;
            if (value.Trim() != string.Empty)
            {
                DataViewModel.NewProject.Name = value.Trim();
                OnPropertyChanged();
            }
        }
    }

    [RelayCommand]
    private void AddProject()
    {
        DataViewModel.AddProject();
        DataViewModel.CurrentView = new ProjectsKanbanViewModel();
    }
}