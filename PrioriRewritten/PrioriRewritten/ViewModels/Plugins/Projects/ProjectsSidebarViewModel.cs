using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace PrioriRewritten.ViewModels.Plugins.Projects;

public partial class ProjectsSidebarViewModel : ViewModelBase
{
    [ObservableProperty] private static ProjectsDataViewModel _dataViewModel = ProjectsDataViewModel.Instance;

    [RelayCommand]
    private void ChangeViewAddProject()
    {
        DataViewModel.CurrentView = new ProjectsAddProjectViewModel();
    }
}