using System.Collections.ObjectModel;
using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.Models.Plugins.Projects;
using PrioriRewritten.ViewModels.Base;

namespace PrioriRewritten.ViewModels.Plugins.Projects;

public partial class ProjectsDataViewModel : ViewModelBase
{
    [ObservableProperty] private ViewModelBase _currentSubView = null!;
    [ObservableProperty] private ViewModelBase _currentView = new NotImplementedViewModel();
    [ObservableProperty] private ProjectsProjectModel _newProject = new();
    [ObservableProperty] private ObservableCollection<ProjectsProjectModel> _projects = new();
    [ObservableProperty] private ProjectsProjectModel _selectedProject = new();
    [ObservableProperty] private ProjectsKanbanBoardModel _selectedBoard = new();
    public static ProjectsDataViewModel Instance { get; } = new();

    public void AddProject()
    {
        if (NewProject.Name.Trim() != string.Empty)
        {
            Projects.Add(NewProject);
            SelectedProject = Projects[Projects.IndexOf(NewProject)];
            NewProject = new ProjectsProjectModel();
        }
    }
}