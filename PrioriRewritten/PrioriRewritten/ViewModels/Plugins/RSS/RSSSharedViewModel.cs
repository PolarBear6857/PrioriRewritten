using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.ViewModels.Base;

namespace PrioriRewritten.ViewModels.Plugins.RSS;

public partial class RSSSharedViewModel : ViewModelBase
{
    [ObservableProperty] private ViewModelBase _currentView = new NotImplementedViewModel();
    public static RSSSharedViewModel Instance { get; } = new();


    [RelayCommand]
    private void ChangeViewCombined()
    {
        if (CurrentView is not RSSCombinedViewModel) CurrentView = new RSSCombinedViewModel();
    }

    [RelayCommand]
    private void ChangeViewAddAccount()
    {
        if (CurrentView is not RSSAddAccountViewModel) CurrentView = new RSSAddAccountViewModel();
    }
}