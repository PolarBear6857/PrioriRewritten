using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.Models.Plugins.RSS;
using PrioriRewritten.Models.Plugins.RSS.Enums;

namespace PrioriRewritten.ViewModels.Plugins.RSS;

public partial class RSSSharedAccountsViewModel : ViewModelBase
{
    [ObservableProperty] private List<RSSAccountEnums> _accountEnums =
        Enum.GetValues(typeof(RSSAccountEnums)).Cast<RSSAccountEnums>().ToList();

    [ObservableProperty] private bool _isAccountSelected;

    [ObservableProperty] private ObservableCollection<RSSAccountModel> _rssAccounts = new();
    private RSSAccountModel? _selectedAccount;
    public static RSSSharedAccountsViewModel Instance { get; } = new();

    public RSSAccountModel? SelectedAccount
    {
        get => _selectedAccount;
        set
        {
            _selectedAccount = value;
            IsAccountSelected = true;
            OnPropertyChanged();
        }
    }
}