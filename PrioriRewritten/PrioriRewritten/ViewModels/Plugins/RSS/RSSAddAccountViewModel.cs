using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.Models.Plugins.RSS;
using PrioriRewritten.Models.Plugins.RSS.Enums;

namespace PrioriRewritten.ViewModels.Plugins.RSS;

public partial class RSSAddAccountViewModel : ViewModelBase
{
    [ObservableProperty] private RSSAccountModel _newAccount = new();

    [ObservableProperty]
    private RSSSharedAccountsViewModel _sharedAccountsViewModel = RSSSharedAccountsViewModel.Instance;

    public RSSAccountEnums ConnectionType
    {
        get => NewAccount.Type;
        set
        {
            NewAccount.Type = value;
            OnPropertyChanged();
        }
    }

    [RelayCommand]
    private void TestConnection()
    {
    }

    [RelayCommand]
    private void AddAccount()
    {
    }
}