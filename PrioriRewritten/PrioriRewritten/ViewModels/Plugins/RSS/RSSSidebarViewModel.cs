using CommunityToolkit.Mvvm.ComponentModel;

namespace PrioriRewritten.ViewModels.Plugins.RSS;

public partial class RSSSidebarViewModel : ViewModelBase
{
    [ObservableProperty]
    private RSSSharedAccountsViewModel _sharedAccountsViewModel = RSSSharedAccountsViewModel.Instance;

    [ObservableProperty] private RSSSharedViewModel _sharedViewModel = RSSSharedViewModel.Instance;
}