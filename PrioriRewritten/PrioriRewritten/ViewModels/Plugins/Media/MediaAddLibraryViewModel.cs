using System;
using System.Collections.Generic;
using System.Linq;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.Models.Plugins.Media;

namespace PrioriRewritten.ViewModels.Plugins.Media;

public partial class MediaAddLibraryViewModel : ViewModelBase
{
    private readonly MediaSharedViewModel _mediaSharedViewModel = MediaSharedViewModel.Instance;
    [ObservableProperty] private bool _createButtonVisible;

    private string _name = string.Empty;
    [ObservableProperty] private int _selectedConnectionIndex;
    [ObservableProperty] private int _selectedTypeIndex;
    [ObservableProperty] private List<string> _typeEnums = Enum.GetNames(typeof(MediaTypeEnums)).ToList();

    public MediaSharedAccountsViewModel SharedAccountsViewModel => MediaSharedAccountsViewModel.Instance;


    public string Name
    {
        get => _name;
        set
        {
            _name = value;
            CreateButtonVisible = Name.Trim() != string.Empty;
        }
    }

    [RelayCommand]
    private void AddLibrary()
    {
        // TODO FIX TYPE SELECT
        _mediaSharedViewModel.MediaLibraries.Add(new MediaLibraryModel(Name, MediaTypeEnums.Movie));
        _mediaSharedViewModel.SelectedLibrary =
            _mediaSharedViewModel.MediaLibraries[^1];
        _mediaSharedViewModel.ChangeViewSelectedLibrary();
    }

    [RelayCommand]
    private void Cancel()
    {
        if (_mediaSharedViewModel.CurrentView is not MediaDashboardViewModel)
            _mediaSharedViewModel.CurrentView = new MediaDashboardViewModel();
    }
}