using System.Collections.ObjectModel;
using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.Models.Plugins.Media;

namespace PrioriRewritten.ViewModels.Plugins.Media;

public partial class MediaSharedViewModel : ViewModelBase
{
    [ObservableProperty] private ViewModelBase _currentView = new MediaDashboardViewModel();

    [ObservableProperty] private ObservableCollection<MediaLibraryModel> _mediaLibraries = new();
    private MediaManager _mediaManager = new();
    [ObservableProperty] private MediaLibraryModel _selectedLibrary = new("%LIBRARY__NAME", MediaTypeEnums.Movie);

    public static MediaSharedViewModel Instance { get; } = new();


    public void ChangeViewSelectedLibrary()
    {
        if (SelectedLibrary!.Type == MediaTypeEnums.Movie) CurrentView = new MediaMovieLibraryViewModel();
    }
}