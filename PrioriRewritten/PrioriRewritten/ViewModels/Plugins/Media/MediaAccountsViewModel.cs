using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace PrioriRewritten.ViewModels.Plugins.Media;

public partial class MediaAccountsViewModel : ViewModelBase
{
    [ObservableProperty]
    private MediaSharedAccountsViewModel _sharedAccountsViewModel = MediaSharedAccountsViewModel.Instance;

    [ObservableProperty] private MediaSharedViewModel _sharedViewModel = MediaSharedViewModel.Instance;

    [RelayCommand]
    private void ChangeViewAddConnection()
    {
        if (SharedViewModel.CurrentView is not MediaAddAccountViewModel)
            SharedViewModel.CurrentView = new MediaAddAccountViewModel();
    }
}