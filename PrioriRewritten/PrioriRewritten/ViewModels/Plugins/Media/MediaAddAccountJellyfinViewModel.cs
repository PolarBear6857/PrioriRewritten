using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.Managers.Plugins.Media;
using PrioriRewritten.Models.Plugins.Media.Connections;
using PrioriRewritten.Models.Plugins.Media.Enums;

namespace PrioriRewritten.ViewModels.Plugins.Media;

public partial class MediaAddAccountJellyfinViewModel : ViewModelBase
{
    private readonly MediaSharedViewModel _sharedViewModel = MediaSharedViewModel.Instance;

    [ObservableProperty] private List<MediaConnectionEnums> _allConnectionEnums =
        Enum.GetValues(typeof(MediaConnectionEnums)).Cast<MediaConnectionEnums>().ToList();

    [ObservableProperty] private bool _canTestConnection;

    [ObservableProperty] private MediaConnectionStatusEnums _connectionStatus = MediaConnectionStatusEnums.Unknown;


    [ObservableProperty] private MediaAccountModel _newAccount = new();

    [ObservableProperty]
    private MediaSharedAccountsViewModel _sharedAccountsViewModel = MediaSharedAccountsViewModel.Instance;

    public List<MediaConnectionEnums> AddableConnectionEnums => AllConnectionEnums
        .Where(enumValue => enumValue != MediaConnectionEnums.Local).ToList();

    private bool _canCreateConnection => ConnectionStatus == MediaConnectionStatusEnums.Ok;

    public MediaConnectionEnums ConnectionType
    {
        get => NewAccount.Type;
        set
        {
            NewAccount.Type = value;
            CanTestConnection = value != MediaConnectionEnums.Local;
        }
    }


    [RelayCommand]
    private void AddConnection()
    {
        SharedAccountsViewModel.AddConnection(NewAccount);
    }

    [RelayCommand]
    private async Task TestConnection()
    {
        if (await MediaConnectionManager.TestConnection(NewAccount))
            ConnectionStatus = MediaConnectionStatusEnums.Ok;
        else
            ConnectionStatus = MediaConnectionStatusEnums.Error;
    }


    [RelayCommand]
    private void Cancel()
    {
        if (_sharedViewModel.CurrentView is not MediaAddAccountViewModel)
            _sharedViewModel.CurrentView = new MediaAddAccountViewModel();
    }
}