using CommunityToolkit.Mvvm.Input;

namespace PrioriRewritten.ViewModels.Plugins.Media;

public partial class MediaAddAccountViewModel : ViewModelBase
{
    private readonly MediaSharedViewModel _sharedViewModel = MediaSharedViewModel.Instance;

    [RelayCommand]
    private void ChangeViewJellyfin()
    {
        if (_sharedViewModel.CurrentView is not MediaAddAccountJellyfinViewModel)
            _sharedViewModel.CurrentView = new MediaAddAccountJellyfinViewModel();
    }

    [RelayCommand]
    private void Cancel()
    {
        if (_sharedViewModel.CurrentView is not MediaAccountsViewModel)
            _sharedViewModel.CurrentView = new MediaAccountsViewModel();
    }
}