using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace PrioriRewritten.ViewModels.Plugins.Media;

public partial class MediaSidebarViewModel : ViewModelBase
{
    [ObservableProperty] private MediaSharedViewModel _sharedViewModel = MediaSharedViewModel.Instance;

    [RelayCommand]
    private void ChangeViewAddLibrary()
    {
        if (SharedViewModel.CurrentView is not MediaAddLibraryViewModel)
            SharedViewModel.CurrentView = new MediaAddLibraryViewModel();
    }

    [RelayCommand]
    private void ChangeViewDashboard()
    {
        if (SharedViewModel.CurrentView is not MediaDashboardViewModel)
            SharedViewModel.CurrentView = new MediaDashboardViewModel();
    }

    [RelayCommand]
    private void ChangeViewSummary()
    {
        if (SharedViewModel.CurrentView is not MediaSummaryViewModel)
            SharedViewModel.CurrentView = new MediaSummaryViewModel();
    }

    [RelayCommand]
    private void ChangeViewAccounts()
    {
        if (SharedViewModel.CurrentView is not MediaAccountsViewModel)
            SharedViewModel.CurrentView = new MediaAccountsViewModel();
    }
}