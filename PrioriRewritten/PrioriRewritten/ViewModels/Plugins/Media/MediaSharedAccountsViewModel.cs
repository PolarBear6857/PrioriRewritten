using System.Collections.Generic;
using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.Models.Plugins.Media.Connections;
using PrioriRewritten.Models.Plugins.Media.Enums;

namespace PrioriRewritten.ViewModels.Plugins.Media;

public partial class MediaSharedAccountsViewModel : ViewModelBase
{
    [ObservableProperty] private List<string> _availableConnectionEnums =
        [MediaConnectionEnums.Local.ToString()];

    [ObservableProperty] private List<MediaAccountModel> _connections =
    [
        new MediaAccountModel
        {
            Name = "Local",
            Type = MediaConnectionEnums.Local
        }
    ];

    public static MediaSharedAccountsViewModel Instance { get; } = new();

    public void AddConnection(MediaAccountModel account)
    {
        Connections.Add(account);
        AvailableConnectionEnums.Add(account.Type.ToString());
    }
}