using System.Collections.Generic;
using System.Linq;
using CommunityToolkit.Mvvm.ComponentModel;

namespace PrioriRewritten.ViewModels.Plugins.Media;

public partial class MediaSummaryViewModel : ViewModelBase
{
    private int _selectedIndex;
    [ObservableProperty] private MediaSharedViewModel _sharedViewModel = MediaSharedViewModel.Instance;

    public IEnumerable<string> LibraryNames =>
        from Library in SharedViewModel.MediaLibraries
        select Library.Name;

    public int SelectedIndex
    {
        get => SharedViewModel.MediaLibraries.Count == 0 ? 0 : _selectedIndex;
        set => _selectedIndex = value;
    }
}