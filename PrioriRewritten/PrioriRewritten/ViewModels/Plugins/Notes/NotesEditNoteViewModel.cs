using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.Managers.Base;

namespace PrioriRewritten.ViewModels.Plugins.Notes;

public partial class NotesEditNoteViewModel : ViewModelBase
{
    private readonly string _editOffIconPath = "/Assets/Icons/White/edit-off.svg";
    private readonly string _editOnIconPath = "/Assets/Icons/White/edit.svg";
    [ObservableProperty] private ColorManager _colorManager = ColorManager.Instance;
    [ObservableProperty] private string? _editIconPath;
    [ObservableProperty] private NotesDataViewModel _notesData = NotesDataViewModel.Instance;
    [ObservableProperty] private ViewModelBase _tagsView = new NotesTagsViewModel();

    public NotesEditNoteViewModel()
    {
        ChangeIcon();
    }


    [RelayCommand]
    private void Back()
    {
        var model = new NotesHeadViewModel();
        model.Initialize();
    }

    [RelayCommand]
    private void FlipEditMode()
    {
        NotesData.SelectedNote.ReadOnly = !NotesData.SelectedNote.ReadOnly;
        ChangeIcon();
    }

    private void ChangeIcon()
    {
        EditIconPath = NotesData.SelectedNote.ReadOnly ? _editOffIconPath : _editOnIconPath;
    }
}