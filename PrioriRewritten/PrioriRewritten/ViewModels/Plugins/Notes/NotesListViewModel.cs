using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace PrioriRewritten.ViewModels.Plugins.Notes;

public partial class NotesListViewModel : ViewModelBase
{
    [ObservableProperty] private NotesDataViewModel _notesData = NotesDataViewModel.Instance;

    [RelayCommand]
    private void AddNote()
    {
        NotesData.AddNote();
    }

    [RelayCommand]
    private void ChangeViewCards()
    {
        NotesData.IsListViewSelected = false;
        NotesData.CurrentView = new NotesCardsViewModel();
    }
    
    
    [RelayCommand]
    private void ClearSelectedTag()
    {
        NotesData.SelectedTag = null!;
    }
}