using CommunityToolkit.Mvvm.ComponentModel;

namespace PrioriRewritten.ViewModels.Plugins.Notes;

public partial class NotesHeadViewModel : ViewModelBase
{
    [ObservableProperty] private NotesDataViewModel _notesData = NotesDataViewModel.Instance;

    public NotesHeadViewModel()
    {
        Initialize();
    }

    public void Initialize()
    {
        if (NotesData.IsListViewSelected)
            NotesData.CurrentView = new NotesListViewModel();
        else
            NotesData.CurrentView = new NotesCardsViewModel();
    }
}