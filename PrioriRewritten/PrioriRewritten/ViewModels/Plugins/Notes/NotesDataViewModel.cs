using System;
using System.Collections.ObjectModel;
using System.Linq;
using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.Models.Plugins.Notes;

namespace PrioriRewritten.ViewModels.Plugins.Notes;

public partial class NotesDataViewModel : ViewModelBase
{
    [ObservableProperty] private ObservableCollection<NotesNoteModel> _allNotes = new();
    [ObservableProperty] private ObservableCollection<NotesNoteModel> _filteredNotes = new();
    [ObservableProperty] private ObservableCollection<NotesTagModel> _allTags = new();
    [ObservableProperty] private ViewModelBase _currentView = null!;
    [ObservableProperty] private NotesNoteModel _newNote = new();
    [ObservableProperty] private NotesTagModel _newTag = new();
    [ObservableProperty] private NotesNoteModel _selectedNote = new();
    private NotesTagModel _selectedTag = null!;
    [ObservableProperty] private bool isListViewSelected;

    public NotesDataViewModel()
    {
        FilterNotes();
    }

    private void FilterNotes()
    {
        FilteredNotes = new();
        foreach (NotesNoteModel note in AllNotes)
        {
            if (SelectedTag == null)
            {
                FilteredNotes.Add(note);
            }
            else
            {
                foreach (NotesTagModel tag in note.Tags)
                {
                    if (tag.Name == SelectedTag.Name)
                    {
                        FilteredNotes.Add(note);
                    }
                }
            }
        }
    }

    public static NotesDataViewModel Instance { get; } = new();

    public NotesTagModel SelectedTag
    {
        get => _selectedTag;
        set
        {
            _selectedTag = value;
            OnPropertyChanged();
            FilterNotes();
        }
    }

    public void AddNote()
    {
        AllNotes.Add(NewNote);
        NewNote = new NotesNoteModel();
        FilterNotes();
    }

    public void AddTag()
    {
        if (NewTag.Name.Trim() == string.Empty) return;

        var tmpName = NewTag.Name.Trim().ToUpper();
        foreach (var tag in AllTags)
            if (tmpName == tag.Name)
                return;

        NewTag.Name = NewTag.Name.Trim().ToUpper();
        AllTags.Add(NewTag);
        NewTag = new NotesTagModel();
    }
}