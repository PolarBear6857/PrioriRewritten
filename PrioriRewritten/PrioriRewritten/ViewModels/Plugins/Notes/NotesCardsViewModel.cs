using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace PrioriRewritten.ViewModels.Plugins.Notes;

public partial class NotesCardsViewModel : ViewModelBase
{
    [ObservableProperty] private NotesDataViewModel _notesData = NotesDataViewModel.Instance;

    [RelayCommand]
    private void AddNote()
    {
        NotesData.AddNote();
    }

    [RelayCommand]
    private void ChangeViewList()
    {
        NotesData.IsListViewSelected = true;
        NotesData.CurrentView = new NotesListViewModel();
    }

    [RelayCommand]
    private void ClearSelectedTag()
    {
        NotesData.SelectedTag = null!;
    }
}