using System.Collections.ObjectModel;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.Managers.Base;
using PrioriRewritten.Models.Plugins.Notes;

namespace PrioriRewritten.ViewModels.Plugins.Notes;

public partial class NotesTagsViewModel : ViewModelBase
{
    [ObservableProperty] private ColorManager _colorManager = ColorManager.Instance;
    [ObservableProperty] private NotesDataViewModel _notesData = NotesDataViewModel.Instance;
    public NotesTagsViewModel()
    {
        NotesData.NewTag = new NotesTagModel();
    }

    [RelayCommand]
    private void AddTag()
    {
        NotesData.AddTag();
    }
}