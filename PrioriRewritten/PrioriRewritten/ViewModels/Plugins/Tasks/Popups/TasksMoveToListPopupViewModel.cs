using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.ViewModels.Plugins.Tasks.Subviews;

namespace PrioriRewritten.ViewModels.Plugins.Tasks.Popups;

public partial class TasksMoveToListPopupViewModel : ViewModelBase
{
    [ObservableProperty] private TasksDataViewModel _tasksData = TasksDataViewModel.Instance;

    [RelayCommand]
    private void MoveTask()
    {
        TasksData.SelectedList.Tasks.Add(TasksData.SelectedTask);
        TasksData.AllTasks.Remove(TasksData.SelectedTask);
        TasksData.SelectedTask = new();
        TasksData.SelectedView = new TasksListViewModel();
        TasksData.SortTasksToGroups();
    }
}