using System;
using System.Collections.ObjectModel;
using System.Linq;
using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.Models.Plugins.Tasks;
using PrioriRewritten.ViewModels.Plugins.Tasks.Subviews;

namespace PrioriRewritten.ViewModels.Plugins.Tasks;

public partial class TasksDataViewModel : ViewModelBase
{
    [ObservableProperty] private ObservableCollection<TasksTaskModel> _allTasks = new();

    // Views
    [ObservableProperty] private ViewModelBase _currentView = null!;
    [ObservableProperty] private ViewModelBase _dateView = null!;
    [ObservableProperty] private string _formattedDate = string.Empty;
    [ObservableProperty] private TasksTaskModel _newTask = new();
    [ObservableProperty] private TasksListModel _selectedList = new();

    // Selected
    [ObservableProperty] private TasksTaskModel _selectedTask = new();
    [ObservableProperty] private ViewModelBase _selectedTaskView = null!;
    [ObservableProperty] private ViewModelBase _selectedView = null!;
    [ObservableProperty] private ObservableCollection<TasksGroupModel> _taskGroups = new();
    [ObservableProperty] private TasksTaskGroupsViewModel _taskGroupsView = null!;

    // Tasks
    [ObservableProperty] private ObservableCollection<TasksListModel> _tasksLists = new();

    public Func<TasksTaskModel, string> Filter;
    public static TasksDataViewModel Instance { get; } = new();

    [ObservableProperty] private int _taskCount;
    [ObservableProperty] private int _allTaskCount;


    public DateTime NewTaskDueDateTime
    {
        get => NewTask.DueDateTime ?? DateTime.Today.AddDays(1);
        set
        {
            if (NewTask.DueDateTime != value)
            {
                NewTask.DueDateTime = value;
                OnPropertyChanged();
                FormattedDate = FormatDate(NewTask.DueDateTime.Value);
                OnPropertyChanged();
                NewTaskTime = value.TimeOfDay;
                OnPropertyChanged();
            }
        }
    }

    public TimeSpan? NewTaskTime
    {
        get => NewTask.DueDateTime?.TimeOfDay;
        set
        {
            if (value != null && (!NewTask.DueDateTime.HasValue || NewTask.DueDateTime.Value.TimeOfDay != value.Value))
            {
                NewTask.DueDateTime = NewTask.DueDateTime?.Date + value;
                OnPropertyChanged();
                if (NewTask.DueDateTime != null) FormattedDate = FormatDate(NewTask.DueDateTime.Value);
                OnPropertyChanged();
            }
        }
    }

    public string FormatDate(DateTime time)
    {
        if (time == DateTime.Today.Date) return "Today";

        if (time == DateTime.Today.Date.AddDays(1)) return "Tomorrow";

        return NewTask.DueDateTime.Value.Date.ToString("d");
    }

    public void SetDefault()
    {
        SelectedTask = new TasksTaskModel();
        SelectedTaskView = new TasksNoTaskSelectedViewModel();
        TaskGroups = new ObservableCollection<TasksGroupModel>();
        NewTask = new TasksTaskModel();
        DateView = new TasksTimePickerViewModel();
        if (NewTask.DueDateTime != null) FormattedDate = FormatDate(NewTask.DueDateTime.Value);
    }

    public void AddTask()
    {
        if (NewTask.Title.Trim() != string.Empty)
        {
            if (SelectedView is TasksListViewModel)
            {
                SelectedList.Tasks.Add(NewTask);
                SelectedList.TaskCount += 1;
                SelectedTask = SelectedList.Tasks[SelectedList.Tasks.IndexOf(NewTask)];
            }
            else
            {
                AllTasks.Add(NewTask);
                TaskCount += 1;
                SelectedTask = AllTasks[AllTasks.IndexOf(NewTask)];
            }

            NewTask = new TasksTaskModel();
            if (SelectedView is TasksTodayTasksViewModel)
            {
                NewTaskDueDateTime = DateTime.Today;
            }

            if (NewTask.DueDateTime != null) FormattedDate = FormatDate(NewTask.DueDateTime.Value);
            SelectedTaskView = new TasksTaskSelectedViewModel();
            OnPropertyChanged();
        }
    }

    public void SortTasksToGroups()
    {
        if (SelectedView is TasksListViewModel)
        {
            TaskGroups = new ObservableCollection<TasksGroupModel>();
            var groupedTasks = SelectedList.Tasks
                .GroupBy(Filter)
                .Select(group => new TasksGroupModel
                    { Name = group.Key, Tasks = new ObservableCollection<TasksTaskModel>(group) });
            foreach (var group in groupedTasks)
                if (group.Name != string.Empty)
                    TaskGroups.Add(group);
        }
        else
        {
            TaskGroups = new ObservableCollection<TasksGroupModel>();
            var groupedTasks = AllTasks
                .GroupBy(Filter)
                .Select(group => new TasksGroupModel
                    { Name = group.Key, Tasks = new ObservableCollection<TasksTaskModel>(group) });
            foreach (var group in groupedTasks)
                if (group.Name != string.Empty)
                    TaskGroups.Add(group);
        }

        OnPropertyChanged();
    }

    public void DeleteTask()
    {
        SelectedTask.IsDeleted = true;
        SortTasksToGroups();
        SelectedView = SelectedView is not TasksListViewModel ? new TasksDeletedViewModel() : new TasksListViewModel();
    }

    public void WontDoTask()
    {
        SelectedTask.IsWontDo = true;
        SortTasksToGroups();
        SelectedView = SelectedView is not TasksListViewModel
            ? new TasksWontDoTasksViewModel()
            : new TasksListViewModel();
    }

    public void ReopenTask()
    {
        SelectedTask.IsWontDo = false;
        SortTasksToGroups();
        SelectedView = SelectedView is not TasksListViewModel ? new TasksInboxViewModel() : new TasksListViewModel();
    }

    public void RestoreTask()
    {
        SelectedTask.IsDeleted = false;
        SortTasksToGroups();
        SelectedView = SelectedView is not TasksListViewModel ? new TasksInboxViewModel() : new TasksListViewModel();
    }

    public void EraseTask()
    {
        if (SelectedView is TasksListViewModel)
        {
            SelectedList.Tasks.Remove(SelectedTask);
            SelectedList.TaskCount -= 1;
        }
        else
        {
            AllTasks.Remove(SelectedTask);
            TaskCount -= 1;
        }

        SortTasksToGroups();
        SelectedView = SelectedView is not TasksListViewModel ? new TasksDeletedViewModel() : new TasksListViewModel();
    }

    public void CalculateTotalTaskCount()
    {
        int total = TaskCount;
        foreach (var list in TasksLists)
        {
            total += list.TaskCount;
        }

        AllTaskCount = total;
    }
}