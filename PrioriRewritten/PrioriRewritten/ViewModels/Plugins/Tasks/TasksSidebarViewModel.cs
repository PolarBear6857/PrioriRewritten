using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.Models.Plugins.Tasks;
using PrioriRewritten.ViewModels.Plugins.Tasks.Subviews;

namespace PrioriRewritten.ViewModels.Plugins.Tasks;

public partial class TasksSidebarViewModel : ViewModelBase
{
    [ObservableProperty] private TasksDataViewModel _tasksData = TasksDataViewModel.Instance;

    public TasksSidebarViewModel()
    {
        TasksData.CurrentView = new TasksHeadViewModel();
    }

    private void SetHeadView()
    {
        if (TasksData.CurrentView is not TasksHeadViewModel) TasksData.CurrentView = new TasksHeadViewModel();
        TasksData.SortTasksToGroups();
    }

    [RelayCommand]
    private void ChangeViewAllTasks()
    {
        TasksData.SelectedView = new TasksInboxViewModel();
        SetHeadView();
    }

    [RelayCommand]
    private void ChangeViewTodayTasks()
    {
        TasksData.SelectedView = new TasksTodayTasksViewModel();
        SetHeadView();
    }

    [RelayCommand]
    private void ChangeViewTomorrowTasks()
    {
        TasksData.SelectedView = new TasksTomorrowTasksViewModel();
        SetHeadView();
    }

    [RelayCommand]
    private void ChangeViewCompletedTasks()
    {
        TasksData.SelectedView = new TasksCompletedViewModel();
        SetHeadView();
    }

    [RelayCommand]
    private void ChangeViewWontDoTasks()
    {
        TasksData.SelectedView = new TasksWontDoTasksViewModel();
        SetHeadView();
    }

    [RelayCommand]
    private void ChangeViewTrashTasks()
    {
        TasksData.SelectedView = new TasksDeletedViewModel();
        SetHeadView();
    }

    [RelayCommand]
    private void ChangeViewAddList()
    {
        TasksData.CurrentView = new TasksAddListViewModel();
    }

    [RelayCommand]
    private void ChangeViewAddTag()
    {
        TasksData.CurrentView = new TasksAddTagViewModel();
    }

    [RelayCommand]
    private void ChangeViewSummary()
    {
        TasksData.CurrentView = new TasksSummaryViewModel();
    }

    [RelayCommand]
    private void TestList()
    {
        TasksData.TasksLists.Add(new TasksListModel
        {
            Name = "TEST"
        });
    }
}