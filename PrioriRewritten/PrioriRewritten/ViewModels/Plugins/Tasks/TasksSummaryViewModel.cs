using CommunityToolkit.Mvvm.Input;

namespace PrioriRewritten.ViewModels.Plugins.Tasks;

public partial class TasksSummaryViewModel : ViewModelBase
{
    private readonly TasksDataViewModel _tasksData = TasksDataViewModel.Instance;

    [RelayCommand]
    private void Back()
    {
        _tasksData.CurrentView = new TasksHeadViewModel();
    }
}