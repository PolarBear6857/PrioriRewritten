using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.ViewModels.Plugins.Tasks.Subviews;

namespace PrioriRewritten.ViewModels.Plugins.Tasks;

public partial class TasksHeadViewModel : ViewModelBase
{
    [ObservableProperty] private TasksDataViewModel _tasksData = TasksDataViewModel.Instance;

    public TasksHeadViewModel()
    {
        if (TasksData.SelectedView is null) TasksData.SelectedView = new TasksInboxViewModel();
        TasksData.SelectedTaskView = new TasksNoTaskSelectedViewModel();
    }
}