using System;
using Avalonia;
using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.Models.Plugins.Tasks;

namespace PrioriRewritten.ViewModels.Plugins.Tasks.Subviews;

public partial class TasksTomorrowTasksViewModel : ViewModelBase
{
    [ObservableProperty] private static TasksDataViewModel _tasksData = TasksDataViewModel.Instance;
    [ObservableProperty] private Thickness _addTaskBorderThickness;
    [ObservableProperty] private bool _canAddTask;
    [ObservableProperty] private bool _dateButtonVisible;

    public TasksTomorrowTasksViewModel()
    {
        TasksData.SetDefault();
        TasksData.NewTaskDueDateTime = DateTime.Today.AddDays(1);
        TasksData.Filter = GetTaskCategory;
        TasksData.TaskGroupsView = new TasksTaskGroupsViewModel();
    }

    public string NewTaskName
    {
        get => TasksData.NewTask.Title;
        set
        {
            TasksData.NewTask.Title = value;
            CanAddTask = value.Trim() != string.Empty;
            OnPropertyChanged();
        }
    }

    private string GetTaskCategory(TasksTaskModel task)
    {
        if (task is { IsDeleted: false, IsWontDo: false })
        {
            var today = DateTime.Today;
            if (task.DueDateTime == today.AddDays(1))
            {
                if (task.IsCompleted) return "Completed";
                return "Tomorrow";
            }
        }


        return string.Empty;
    }

    public void AddTask()
    {
        TasksData.AddTask();
        TasksData.SortTasksToGroups();
    }
}