using System;
using Avalonia;
using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.Models.Plugins.Tasks;

namespace PrioriRewritten.ViewModels.Plugins.Tasks.Subviews;

public partial class TasksListViewModel : ViewModelBase
{
    [ObservableProperty] private static TasksDataViewModel _tasksData = TasksDataViewModel.Instance;
    [ObservableProperty] private Thickness _addTaskBorderThickness;
    [ObservableProperty] private bool _canAddTask;
    [ObservableProperty] private bool _dateButtonVisible;
    [ObservableProperty] private bool _canEditListName;

    public TasksListViewModel()
    {
        TasksData.SetDefault();
        TasksData.NewTaskDueDateTime = DateTime.Today.AddDays(1);
        TasksData.Filter = GetTaskCategory;
        TasksData.SortTasksToGroups();
    }

    public string NewTaskName
    {
        get => TasksData.NewTask.Title;
        set
        {
            TasksData.NewTask.Title = value;
            CanAddTask = value.Trim() != string.Empty;
            OnPropertyChanged();
        }
    }


    private string GetTaskCategory(TasksTaskModel task)
    {
        if (task is { IsCompleted: false, IsWontDo: false, IsDeleted: false })
        {
            if (task.DueDateTime == null) return "No Date";
            var today = DateTime.Today;
            if (task.DueDateTime < today) return "Overdue";
            if (task.IsToday) return "Today";
            if (task.IsTomorrow) return "Tomorrow";
            if (task.DueDateTime <= today.AddDays(7)) return "This Week";
            return "Later";
        }

        if (task.IsCompleted) return "Completed";
        if (task.IsWontDo) return "Won't Do";
        if (task.IsDeleted) return "Deleted";
        return string.Empty;
    }

    public void AddTask()
    {
        TasksData.AddTask();
        TasksData.SortTasksToGroups();
    }
}