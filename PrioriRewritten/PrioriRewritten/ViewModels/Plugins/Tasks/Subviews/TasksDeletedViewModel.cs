using System;
using Avalonia;
using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.Models.Plugins.Tasks;

namespace PrioriRewritten.ViewModels.Plugins.Tasks.Subviews;

public partial class TasksDeletedViewModel : ViewModelBase
{
    [ObservableProperty] private static TasksDataViewModel _tasksData = TasksDataViewModel.Instance;
    [ObservableProperty] private Thickness _addTaskBorderThickness;
    [ObservableProperty] private bool _canAddTask;
    [ObservableProperty] private bool _dateButtonVisible;

    public TasksDeletedViewModel()
    {
        TasksData.SetDefault();
        TasksData.NewTaskDueDateTime = DateTime.Today;
        TasksData.Filter = GetTaskCategory;
        TasksData.TaskGroupsView = new TasksTaskGroupsViewModel();
    }

    private string GetTaskCategory(TasksTaskModel task)
    {
        if (task.IsDeleted)
        {
            if (task.IsToday) return "Today";

            return task.CompletedDateTime.Value.ToLongDateString();
        }

        return string.Empty;
    }
}