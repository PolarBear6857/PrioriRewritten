using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace PrioriRewritten.ViewModels.Plugins.Tasks.Subviews;

public partial class TasksTaskGroupsViewModel : ViewModelBase
{
    [ObservableProperty] private TasksDataViewModel _tasksData = TasksDataViewModel.Instance;

    public bool CanMoveItem => TasksData.TasksLists.Count != 0;

    public TasksTaskGroupsViewModel()
    {
        TasksData.SortTasksToGroups();
    }

    [RelayCommand]
    private void Filter()
    {
        TasksData.SortTasksToGroups();
    }
}