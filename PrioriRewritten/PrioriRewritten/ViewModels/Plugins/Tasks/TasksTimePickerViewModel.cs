using System;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace PrioriRewritten.ViewModels.Plugins.Tasks;

public partial class TasksTimePickerViewModel : ViewModelBase
{
    [ObservableProperty] private TasksDataViewModel _tasksData = TasksDataViewModel.Instance;

    [RelayCommand]
    private void Clear()
    {
        TasksData.NewTaskDueDateTime = DateTime.Today.AddDays(1).Date;
    }
}