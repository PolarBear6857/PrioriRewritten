using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.Models.Plugins.Tasks;

namespace PrioriRewritten.ViewModels.Plugins.Tasks;

public partial class TasksAddListViewModel : ViewModelBase
{
    [ObservableProperty] private bool _canAddList = true;
    [ObservableProperty] private TasksListModel _newList = new();
    [ObservableProperty] private TasksDataViewModel _tasksData = TasksDataViewModel.Instance;

    public string NewListName
    {
        get => NewList.Name;
        set
        {
            CanAddList = true;
            if (value.Trim() == string.Empty) CanAddList = false;

            NewList.Name = value;
        }
    }

    [RelayCommand]
    private void Back()
    {
        TasksData.CurrentView = new TasksHeadViewModel();
    }

    [RelayCommand]
    private void AddList()
    {
        if (CanAddList)
        {
            TasksData.TasksLists.Add(NewList);
            TasksData.SelectedList = TasksData.TasksLists[TasksData.TasksLists.IndexOf(NewList)];
            NewList = new TasksListModel();
        }
    }
}