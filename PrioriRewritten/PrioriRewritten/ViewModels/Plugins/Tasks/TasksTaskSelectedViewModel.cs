using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.Models.Plugins.Tasks.Enums;
using PrioriRewritten.ViewModels.Plugins.Tasks.Subviews;

namespace PrioriRewritten.ViewModels.Plugins.Tasks;

public partial class TasksTaskSelectedViewModel : ViewModelBase
{
    private const string PriorityNoneIconPath = "/Assets/Icons/White/flag.svg";
    private const string PriorityLowIconPath = "/Assets/Icons/Blue/flag-filled.svg";
    private const string PriorityMediumIconPath = "/Assets/Icons/Orange/flag-filled.svg";
    private const string PriorityHighIconPath = "/Assets/Icons/Red/flag-filled.svg";
    [ObservableProperty] private static TasksDataViewModel _tasksData = TasksDataViewModel.Instance;
    [ObservableProperty] private string _priorityIconPath = string.Empty;


    public TasksTaskSelectedViewModel()
    {
        switch (_tasksData.SelectedTask.Priority)
        {
            case TasksPriority.Low:
            {
                SetPriorityLow();
                break;
            }
            case TasksPriority.Medium:
            {
                SetPriorityMedium();
                break;
            }
            case TasksPriority.High:
            {
                SetPriorityHigh();
                break;
            }
            default:
            {
                SetPriorityNone();
                break;
            }
        }
    }

    [RelayCommand]
    private void SetPriorityNone()
    {
        _tasksData.SelectedTask.Priority = TasksPriority.None;
        PriorityIconPath = PriorityNoneIconPath;
    }

    [RelayCommand]
    private void SetPriorityLow()
    {
        _tasksData.SelectedTask.Priority = TasksPriority.Low;
        PriorityIconPath = PriorityLowIconPath;
    }

    [RelayCommand]
    private void SetPriorityMedium()
    {
        _tasksData.SelectedTask.Priority = TasksPriority.Medium;
        PriorityIconPath = PriorityMediumIconPath;
    }

    [RelayCommand]
    private void SetPriorityHigh()
    {
        _tasksData.SelectedTask.Priority = TasksPriority.High;
        PriorityIconPath = PriorityHighIconPath;
    }

    [RelayCommand]
    private void DeleteTask() => TasksData.DeleteTask();

    [RelayCommand]
    private void WontDoTask() => TasksData.WontDoTask();

    [RelayCommand]
    private void ReopenTask() => TasksData.ReopenTask();

    [RelayCommand]
    private void RestoreTask() => TasksData.RestoreTask();

    [RelayCommand]
    private void EraseTask() => TasksData.EraseTask();

    [RelayCommand]
    private void Filter()
    {
        TasksData.SortTasksToGroups();
    }
}