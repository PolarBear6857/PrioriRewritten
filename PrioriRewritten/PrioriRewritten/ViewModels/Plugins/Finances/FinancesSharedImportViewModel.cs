using System.Collections.Generic;
using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.Models.Plugins.Finances;
using PrioriRewritten.Models.Plugins.Finances.Enums;

namespace PrioriRewritten.ViewModels.Plugins.Finances;

public partial class FinancesSharedImportViewModel : ViewModelBase
{
    public readonly FinancesAccountSharedViewModel AccountSharedViewModel = FinancesAccountSharedViewModel.Instance;
    [ObservableProperty] private bool _canShowData;
    [ObservableProperty] private bool _foundAccounts;

    private List<FinancesAccountModel>? _importedAccounts;
    private FinancesAccountModel? _selectedAccount;
    public static FinancesSharedImportViewModel Instance { get; } = new();

    public List<FinancesAccountModel> ImportedAccounts
    {
        get => _importedAccounts;
        set
        {
            _importedAccounts = value;
            FoundAccounts = true;
            OnPropertyChanged();
        }
    }


    public FinancesAccountModel SelectedAccount
    {
        get => _selectedAccount;
        set
        {
            _selectedAccount = value;
            CanShowData = true;
            OnPropertyChanged();
        }
    }

    public void ImportAccount()
    {
        decimal balance = 0;
        foreach (var transaction in SelectedAccount.Transactions)
            if (transaction.Type == FinancesTransactionEnum.Deposit)
                balance += transaction.Amount;
            else if (transaction.Type == FinancesTransactionEnum.Withdraw) balance -= transaction.Amount;

        SelectedAccount.Balance = balance;
        AccountSharedViewModel.AddAccount(SelectedAccount);
    }
}