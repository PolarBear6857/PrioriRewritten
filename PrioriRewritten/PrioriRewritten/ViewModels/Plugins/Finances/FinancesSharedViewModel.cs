using CommunityToolkit.Mvvm.ComponentModel;

namespace PrioriRewritten.ViewModels.Plugins.Finances;

public partial class FinancesSharedViewModel : ViewModelBase
{
    [ObservableProperty] private ViewModelBase _currentView = null!;

    public static FinancesSharedViewModel Instance { get; } = new();
}