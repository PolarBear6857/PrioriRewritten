using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.ViewModels.Base.Data.Finances;

namespace PrioriRewritten.ViewModels.Plugins.Finances;

public partial class FinancesSidebarViewModel : ViewModelBase
{
    [ObservableProperty] private FinancesAccountSharedViewModel _accountSharedViewModel =
        FinancesAccountSharedViewModel.Instance;

    [ObservableProperty] private FinancesSharedViewModel _sharedViewModel = FinancesSharedViewModel.Instance;

    public FinancesSidebarViewModel()
    {
        ChangeViewDashboard();
    }

    [RelayCommand]
    private void ChangeViewAddAccount()
    {
        SharedViewModel.CurrentView = new FinancesAddAccountViewModel();
    }

    [RelayCommand]
    private void ChangeViewDashboard()
    {
        if (AccountSharedViewModel.IsAccountSelected)
            SharedViewModel.CurrentView = new FinancesDashboardViewModel();
        else
            SharedViewModel.CurrentView = new FinancesNoAccountSelectedViewModel();
    }

    [RelayCommand]
    private void ChangeViewImportData()
    {
        SharedViewModel.CurrentView = new DataFinancesExportViewModel();
    }
}