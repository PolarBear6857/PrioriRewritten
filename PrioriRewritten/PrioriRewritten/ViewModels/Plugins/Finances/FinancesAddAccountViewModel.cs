using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using PrioriRewritten.Models.Plugins.Finances;

namespace PrioriRewritten.ViewModels.Plugins.Finances;

public partial class FinancesAddAccountViewModel : ViewModelBase
{
    [ObservableProperty]
    private FinancesAccountSharedViewModel _accountSharedViewModel = FinancesAccountSharedViewModel.Instance;

    [ObservableProperty] private bool _canCreateAccount = true;

    [ObservableProperty] private FinancesAccountModel _newAccount = new();

    [ObservableProperty] private FinancesSharedViewModel _sharedViewModel = FinancesSharedViewModel.Instance;


    public string Name
    {
        get => NewAccount.Name;
        set
        {
            NewAccount.Name = value.Trim();
            CanCreateAccount = NewAccount.Name != string.Empty;
            OnPropertyChanged();
        }
    }


    [RelayCommand]
    private void AddAccount()
    {
        if (NewAccount.Name != string.Empty && NewAccount.Currency != null)
        {
            AccountSharedViewModel.AddAccount(NewAccount);
            SharedViewModel.CurrentView = new FinancesDashboardViewModel();
        }
    }
}