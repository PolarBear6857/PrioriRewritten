using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using CommunityToolkit.Mvvm.ComponentModel;
using PrioriRewritten.Models.Plugins.Finances;
using PrioriRewritten.Models.Plugins.Finances.Enums;

namespace PrioriRewritten.ViewModels.Plugins.Finances;

public partial class FinancesAccountSharedViewModel : ViewModelBase
{
    [ObservableProperty] private ObservableCollection<FinancesAccountModel> _financesAccounts = new();

    [ObservableProperty] private List<FinancesCurrencyEnum> _financesCurrencyEnums =
        Enum.GetValues(typeof(FinancesCurrencyEnum)).Cast<FinancesCurrencyEnum>().ToList();

    [ObservableProperty] private bool _isAccountSelected;

    private FinancesAccountModel? _selectedAccount;


    public static FinancesAccountSharedViewModel Instance { get; } = new();

    public FinancesAccountModel? SelectedAccount
    {
        get => _selectedAccount;
        set
        {
            _selectedAccount = value;
            IsAccountSelected = true;
            OnPropertyChanged();
        }
    }

    public void AddAccount(FinancesAccountModel newAccount)
    {
        FinancesAccounts.Add(newAccount);
        SelectedAccount = FinancesAccounts[FinancesAccounts.IndexOf(newAccount)];
    }
}