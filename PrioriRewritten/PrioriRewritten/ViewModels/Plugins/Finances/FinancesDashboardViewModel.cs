using CommunityToolkit.Mvvm.ComponentModel;

namespace PrioriRewritten.ViewModels.Plugins.Finances;

public partial class FinancesDashboardViewModel : ViewModelBase
{
    [ObservableProperty]
    private FinancesAccountSharedViewModel _sharedViewModel = FinancesAccountSharedViewModel.Instance;
}