### 2024-04-11a
- Added custom application icon.
- Changed how links open on platforms.
- Fixed dashboard total task count.
- Improved Priori name scaling.


### 2024-04-09c
- Hid everything that is not "completed".
- Added basic items adding in projects.
- Renamed Projects lists to boards.
- Started working on Tasks summary page.
- Fixed updating tasks groups on task completion 


### 2024-04-09b
- Fixed tasks time picker.
- Improved new task datetime logic.
- Hid Finances, Media, RSS from dashboard and settings.
- Added time label to tasks datetime view.
- Updated README.md to reflect current features and future plans.


### 2024-04-09a
- Fixed settings.
- Added Notes filtering.
- Improved Tasks datetime picker.
- Made it so that you can click anywhere in selected task description.


### 2024-04-07c
- Started working on settings saving and loading.
- Improved logic of MainSidebar.
- Tweaked Tasks datetime picker a bit.


### 2024-04-07b
- Added few icons
- Added Modified datetime to notes.
- Added priority color to Tasks.
- Added moving of tasks to different Lists.
- Added Tasks list delete popup.
- Added Deleted category to lists.
- Modified Tasks priority logic.
- Added bottom bar in Notes edit page.
- Added Notes created and modified datetime to note cards.
- Modified Tasks subpages empty space.
- Made Tasks lists grouping use the shared group system.
- Improved the look of Completed, Won't Do and Deleted tasks.
- Added a way to completely delete task.


### 2024-04-07a
- Refactored project.
- Added ApplicationManager.
- Moved website opening to ApplicationManager.
- Fixed Help page buttons.
- Added Deleting and Renaming on Tasks Lists.
- Added Task list delete popup.


### 2024-04-06a
- Improved the Task model to have better ToString method.
- Modified the Task models Completed param.
- Added status color to Task model.
- Finished the UI of the Help page
- Restructured most of the Tasks grouping
- Made the Tasks grouping into its own view.
- Renamed developer_guide to DEV_GUIDE in documentation.
- Renamed FAQs to FAQ in documentation.
- Removed getting_started.md from documentation.


### 2024-04-04a
- Added Notes list view
- Added Notes view switching
- Improved Notes edit note back button
- Renamed All Notes page to Cards
- Functional lists (kinda)
- Improved sorting tasks to groups


### 2024-03-28c
- Fix About page buttons
- Change About page icons
- Tweak colors a bit
- Started adding Tasks lists
- Added right-click menus to tasks
- Removed Tasks tags for now
- Modified Tasks sidebar for lists


### 2024-03-28b
- Changed Notes menu button icon path
- Added Tasks Deleted page
- Added Tasks WontDo page
- Improved Tasks Today page
- Improved Tasks Tomorrow page
- Improved Tasks Selected page
- Refactored Tasks filtering
- Change Tasks sidebar Button size and spacing
- Added more information each Task stores


### 2024-03-28a
- Changed colors a bit
- Started working on help page
- Added menu button to each note
- Added a way to delete notes
- Fixed Tasks not being able to scroll
- Started working on task priority


### 2024-03-26a
- Moved md files a bit
- Added Today and Tomorrow tasks


### 2024-03-24b
- Fixed back button in about page
- Removed Credits icon
- Tweaked empty page template


### 2024-03-24a
- Created ColorManager
- Added Notes tags count to dashboard
- Updated many pages with new layout template
- Changed main sidebar button padding
- Added note and tag colors
- Improved Projects Kanban UI a bit
- Created Credits page
- Improved Tasks page logic


### 2024-03-21a
- Separated Data logic into Settings -> Data Manager
- Updated template with back button
- Moved Finances Import to Data Manager
- Tweaked main sidebar spacing
- Added checks when adding tags
- Improved tag removing


### 2024-03-20b
- Improved Notes logic
- Started working on Notes tags popup
- Added Notes tag creation and deletion logic


### 2024-03-20a
- Improved Notes list view
- Created Notes editing view
- Added Notes tags
- Improved Projects logic
- Added no lists in Project view


### 2024-03-19b
- Separated Settings into SettingsManager
- Started working on Notes
- Implemented Projects Kanban


### 2024-03-19a
- Started working on Projects
- Improved selected task view


### 2024-03-18b
- Fix color scheme in some views
- Add task creation
- Add task property info
- Change Task model to be observable


### 2024-03-18a
- Change color scheme
- Change icons color
- Made version label bound
- Reformatted code
- Started working on Finances Dashboard
- Started working on Tasks


### 2024-03-15a
- Finished importing of data
- Started processing imported data
- Redesigned Finances Import & Export view
- Created TemplateView
- Modified FinancesAccountModel to include Transactions


### 2024-03-14a
- Made colors configurable
- Started reworking Media Accounts adding
- Started reading excel data for Finances


### 2024-03-11b
- Started working on RSS.
- Updated Media
- Started working on Finances Import.