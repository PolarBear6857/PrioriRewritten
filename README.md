<img src="PrioriRewritten/PrioriRewritten/Assets/Icons/White/Brand/brand-priori.png" alt="Priori logo" width="150"/>


<a href="https://wakatime.com/badge/user/3b2005e9-9840-431f-8059-bd8f75e6ed31/project/018da3b7-4610-4e5b-a9c9-8eba0f9dfcd5"><img src="https://wakatime.com/badge/user/3b2005e9-9840-431f-8059-bd8f75e6ed31/project/018da3b7-4610-4e5b-a9c9-8eba0f9dfcd5.svg" alt="wakatime"></a>

# Priori

Local Tasks, Projects and Notes manager.

## Screenshots

todo

## Features

- Everything is on device. Nothing gets uploaded to the internet. Your data is Your data.
- Data is not encrypted so that if You don't like Priori, You still own Your data and can move it somewhere else.

- Tasks
    - Manage tasks
    - Set priorities
    - Create custom lists
    - Get statistics about your tasks
- Projects
    - Manage projects
    - Create boards
    - Move items between
- Notes
    - Manage notes
    - Organise with tags
    - Choose your view
    - Express yourself with colors
    
    
## Roadmap

### Near future

- Finances
    - Manage accounts
    - Choose your currence
    - Plan your expenses
    - Keep track of your history
    - View statistics about your spending

### 2024

- Media
    - Manage media
    - Connect to Jellyfin, Plex servers
    - Create libraries
    - Filter results

- RSS
    - Manage feeds
    - Connect to FreshRSS
    - Add news to tasks, notes or projects.


## Installation

- Priori is portable so no installation is needed. Download latest version
  from [releases](https://gitlab.com/Anthony0001/PrioriRewritten/-/releases) for your platform and just
  run it.
- Supported platforms are:
    - Windows x86, x64, arm64
    - macOS x64, arm64
    - Linux x64, arm64

## FAQ

[FAQ](https://gitlab.com/Anthony0001/PrioriRewritten/-/blob/master/Documentation/FAQ.md?ref_type=heads)

## Contributing

- Contributions are always welcome!
- Feel free to open an [Issue](https://gitlab.com/Anthony0001/PrioriRewritten/-/issues) or
  a [Merge Request](https://gitlab.com/Anthony0001/PrioriRewritten/-/merge_requests)

## 🔗 Links

- [Wakatime](https://wakatime.com/@Anthony0001)

