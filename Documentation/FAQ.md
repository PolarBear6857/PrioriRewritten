#### Q: Can I use Priori on multiple devices?

A: Yes, Priori is designed to be portable and supports multiple platforms including Windows, macOS, and Linux. However, syncing data across devices requires manual transfer since everything is stored locally for privacy.

#### Q: Is my data safe with Priori?

A: Your data remains entirely on your device. Priori does not encrypt your data, allowing you full control. This means you can easily access and manage your data independently of the application.

#### Q: How can I contribute to Priori?

A: Contributions are welcome! You can contribute by opening issues for bugs or suggestions and submitting merge requests with improvements or new features. Check our contributing guide for more details.

#### Q: Does Priori offer data backup options?

A: Currently, Priori relies on manual backup. You can copy your data folder to a backup location periodically. We are exploring more user-friendly backup solutions for future releases.

#### Q: Can I export my data from Priori?

A: Yes, since your data is not encrypted and stored in a common format, you can export it anytime. This ensures you can migrate to another service without hassle.

#### Q: Are there any plans to introduce encryption for data stored by Priori?

A: While we prioritize transparency and accessibility of data, we are considering optional encryption features for users who request enhanced privacy. Stay tuned for updates.

#### Q: How often is Priori updated with new features or bug fixes?

A: We aim to release updates regularly, addressing both new features and bug fixes. Keep an eye on our release page for the latest versions.

#### Q: Can I request a new feature for Priori?

A: Absolutely! We encourage users to suggest new features by opening an issue on our GitLab page. We value community input to make Priori better for everyone.

#### Q: How do I report a bug I found in Priori?

A: Please report any bugs by opening an issue on our GitLab repository. Include as much detail as possible to help us understand and fix the issue quickly.

#### Q: Is there a user guide or documentation to help me get started with Priori?

A: Yes, we have a comprehensive documentation section that includes a getting started guide, usage tips, and more. You can find it in our Documentation folder on GitLab.